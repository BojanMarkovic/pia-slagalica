/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kontroleri;

import db.HibernateUtil;
import entiteti.Igradana;
import entiteti.Odigranipotezi;
import entiteti.Partije;
import entiteti.Registracija;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@SessionScoped
public class SkockoKontroler implements Serializable {

    private int lokacija = 0, lokacijaRezultata = 0;
    private boolean uslov = false, cekaj = false;
    private ArrayList<Kombinacija> listaKombinacija, listaRezultata;
    private String konacnaKombinacija;
    private String poeni1, poeni2;
    private int vreme = 90;
    private boolean gotovo = true, dodatniUslov = false;
    private boolean gotovaIgra = false;
    private int redniBrPartije = 0;
    private Registracija reg;
    private Partije partija;
    private boolean dodatniPotez = false;

    public void init() {
        if (gotovo == true) {
            redniBrPartije++;
            if (redniBrPartije == 4) {
                partija.setStatus(new Byte("2"));
                SessionFactory sessionF = HibernateUtil.getSessionFactory();
                Session session = sessionF.openSession();
                session.beginTransaction();
                session.update(partija);
                session.getTransaction().commit();
                session.close();
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("gamePage.xhtml");
                } catch (IOException ex) {
                    Logger.getLogger(SkockoKontroler.class.getName()).log(Level.SEVERE, null, ex);
                }
                return;
            }
            gotovo = false;
            dodatniUslov = false;
            vreme = 90;
            konacnaKombinacija = "";

            listaKombinacija = new ArrayList<>();
            listaRezultata = new ArrayList<>();
            for (int i = 0; i < 7; i++) {
                listaKombinacija.add(new Kombinacija());
                listaRezultata.add(new Kombinacija());
            }

            lokacija = 0;
            lokacijaRezultata = 0;
            uslov = false;
            gotovaIgra = false;

        }
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession s = (HttpSession) fc.getExternalContext().getSession(false);
        reg = (Registracija) s.getAttribute("user");
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Partije.class);
        partija = (Partije) query.add(Restrictions.eq("idKorisnika1", reg.getId())).add(Restrictions.eq("status", new Byte("1"))).uniqueResult();
        session.getTransaction().commit();
        if (partija == null) {
            query = session.createCriteria(Partije.class);
            session.beginTransaction();
            partija = (Partije) query.add(Restrictions.eq("idKorisnika2", reg.getId())).add(Restrictions.eq("status", new Byte("1"))).uniqueResult();
            session.getTransaction().commit();
        }
        session.close();
        if (partija == null) {
            poeni1 = "0";
            poeni2 = "0";
            uslov = true;
        } else {
            if (partija.getPotez() != new Byte("2")) {
                if (partija.getIdKorisnika1() != reg.getId()) {
                    uslov = true;
                }
            }
            if (partija.getPotez() == new Byte("2")) {
                if (partija.getIdKorisnika2() == reg.getId()) {
                    uslov = false;
                }
            }
            poeni1 = String.valueOf(partija.getPoeni1());
            poeni2 = String.valueOf(partija.getPoeni2());
        }
    }

    public void sledeciSimbol(String x) {
        if (konacnaKombinacija.isEmpty()) {
            Random temp = new Random(System.currentTimeMillis());
            konacnaKombinacija = temp.nextInt(6) + " ";
            konacnaKombinacija += temp.nextInt(6);
            konacnaKombinacija += " " + temp.nextInt(6);
            konacnaKombinacija += " " + temp.nextInt(6);
        }
        if (!gotovo) {
            if (lokacija == 24) {
                uslov = false;
                predajKombinaciju();
            }
        }
        if (lokacija == 0) {
            dodatniUslov = true;
        }
        int temp = lokacija % 4;
        switch (temp) {
            case 0:
                listaKombinacija.get(lokacija / 4).setA1(x);
                listaKombinacija.get(lokacija / 4).setSlika1(postaviSliku(x));
                break;
            case 1:
                listaKombinacija.get(lokacija / 4).setA2(x);
                listaKombinacija.get(lokacija / 4).setSlika2(postaviSliku(x));
                break;
            case 2:
                listaKombinacija.get(lokacija / 4).setA3(x);
                listaKombinacija.get(lokacija / 4).setSlika3(postaviSliku(x));
                break;
            case 3:
                listaKombinacija.get(lokacija / 4).setA4(x);
                listaKombinacija.get(lokacija / 4).setSlika4(postaviSliku(x));
                break;
        }
        lokacija++;
        if (lokacija % 4 == 0) {
            uslov = true;
            predajKombinaciju();
        }
    }

    public void proveravaj() {
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        if (partija == null) {
            session.beginTransaction();
            Criteria query = session.createCriteria(Partije.class);
            partija = (Partije) query.add(Restrictions.eq("idKorisnika1", reg.getId())).add(Restrictions.eq("status", new Byte("1"))).uniqueResult();
            session.getTransaction().commit();
            if (partija == null) {
                query = session.createCriteria(Partije.class);
                session.beginTransaction();
                partija = (Partije) query.add(Restrictions.eq("idKorisnika2", reg.getId())).add(Restrictions.eq("status", new Byte("1"))).uniqueResult();
                session.getTransaction().commit();
            }
        }

        Criteria query = session.createCriteria(Odigranipotezi.class);
        session.beginTransaction();
        ArrayList<Odigranipotezi> potez1 = (ArrayList<Odigranipotezi>) query.add(Restrictions.eq("idPartije", partija.getId()))
                .add(Restrictions.eq("tipIgre", new Byte("-1"))).list();
        session.getTransaction().commit();
        for (Odigranipotezi p : potez1) {
            int poen = Integer.parseInt(p.getStaJeUradjeno());
            if (p.getIdKorisnika() == partija.getIdKorisnika1()) {
                partija.setPoeni1(partija.getPoeni1() + poen);
            } else {
                partija.setPoeni2(partija.getPoeni2() + poen);
            }
            session.beginTransaction();
            session.update(partija);
            session.getTransaction().commit();
            session.beginTransaction();
            Odigranipotezi pote = (Odigranipotezi) session.merge(p);
            session.delete(pote);
            session.getTransaction().commit();

        }
        boolean radi = false;
        poeni1 = String.valueOf(partija.getPoeni1());
        poeni2 = String.valueOf(partija.getPoeni2());
        if (partija.getPotez() == new Byte("1")) {
            if (partija.getIdKorisnika1() != reg.getId()) {
                radi = true;
            }
        } else {
            if (partija.getIdKorisnika2() != reg.getId()) {
                radi = true;
            }
        }

        if (radi) {
            query = session.createCriteria(Odigranipotezi.class);
            session.beginTransaction();
            ArrayList<Odigranipotezi> potez = (ArrayList<Odigranipotezi>) query.add(Restrictions.eq("idPartije", partija.getId()))
                    .add(Restrictions.eq("tipIgre", new Byte("3"))).list();
            session.getTransaction().commit();
            for (Odigranipotezi p : potez) {
                String[] odradjeno = p.getStaJeUradjeno().split(" ");
                lokacija = Integer.parseInt(odradjeno[0]);
                lokacijaRezultata = Integer.parseInt(odradjeno[0]);

                String t1 = "", t2 = "", t3 = "", t4 = "";
                if (odradjeno[1].compareTo("+") == 0) {
                    t1 = "0";
                } else if (odradjeno[1].compareTo("-") == 0) {
                    t1 = "2";
                } else {
                    t1 = "1";
                }
                if (odradjeno[2].compareTo("+") == 0) {
                    t2 = "0";
                } else if (odradjeno[2].compareTo("-") == 0) {
                    t2 = "2";
                } else {
                    t2 = "1";
                }
                if (odradjeno[3].compareTo("+") == 0) {
                    t3 = "0";
                } else if (odradjeno[3].compareTo("-") == 0) {
                    t3 = "2";
                } else {
                    t3 = "1";
                }
                if (odradjeno[4].compareTo("+") == 0) {
                    t4 = "0";
                } else if (odradjeno[4].compareTo("-") == 0) {
                    t4 = "2";
                } else {
                    t4 = "1";
                }
                listaRezultata.get(lokacijaRezultata % 7).setSlika1(postaviSlikuResenja(t1));
                listaRezultata.get(lokacijaRezultata % 7).setSlika2(postaviSlikuResenja(t2));
                listaRezultata.get(lokacijaRezultata % 7).setSlika3(postaviSlikuResenja(t3));
                listaRezultata.get(lokacijaRezultata % 7).setSlika4(postaviSlikuResenja(t4));

                listaKombinacija.get(lokacija % 7).setSlika1(postaviSliku(odradjeno[5]));
                listaKombinacija.get(lokacija % 7).setSlika2(postaviSliku(odradjeno[6]));
                listaKombinacija.get(lokacija % 7).setSlika3(postaviSliku(odradjeno[7]));
                listaKombinacija.get(lokacija % 7).setSlika4(postaviSliku(odradjeno[8]));

                session.beginTransaction();
                Odigranipotezi pote = (Odigranipotezi) session.merge(p);
                session.delete(pote);
                session.getTransaction().commit();
                if (lokacija == 5) {
                    uslov = false;
                    if (partija.getPotez() != new Byte("2")) {
                        partija.setPotez(new Byte("2"));
                        session.beginTransaction();
                        session.update(partija);
                        session.getTransaction().commit();
                    }
                    query = session.createCriteria(Odigranipotezi.class);
                    session.beginTransaction();
                    potez = (ArrayList<Odigranipotezi>) query.add(Restrictions.eq("idPartije", partija.getId()))
                            .add(Restrictions.eq("tipIgre", new Byte("1"))).list();
                    session.getTransaction().commit();
                    for (int i = 0; i < potez.size(); i++) {
                        session.beginTransaction();
                        pote = (Odigranipotezi) session.merge(potez.get(i));
                        session.delete(pote);
                        session.getTransaction().commit();
                    }
                    gotovo = true;
                    init();
                }
                if ((t1.compareTo("0") == 0) && (t2.compareTo("0") == 0)
                        && (t3.compareTo("0") == 0) && (t4.compareTo("0") == 0)) {
                    uslov = false;
                    if (partija.getPotez() != new Byte("2")) {
                        partija.setPotez(new Byte("2"));
                        session.beginTransaction();
                        session.update(partija);
                        session.getTransaction().commit();
                    }
                    query = session.createCriteria(Odigranipotezi.class);
                    session.beginTransaction();
                    potez = (ArrayList<Odigranipotezi>) query.add(Restrictions.eq("idPartije", partija.getId()))
                            .add(Restrictions.eq("tipIgre", new Byte("1"))).list();
                    session.getTransaction().commit();
                    for (int i = 0; i < potez.size(); i++) {
                        session.beginTransaction();
                        pote = (Odigranipotezi) session.merge(potez.get(i));
                        session.delete(pote);
                        session.getTransaction().commit();
                    }
                    gotovo = true;
                    init();
                }
            }
        }
        session.close();
    }

    public void promenaVremena() {
        if (dodatniUslov) {
            if (vreme >= 0) {
                vreme--;
                if (vreme == 0) {
                    if (!gotovo) {
                        gotovo = true;
                        predajKombinaciju();
                    }
                }
            }
        }
    }

    public void obrisiSimbol() {
        if (lokacija == 0) {
            return;
        }
        lokacija--;
        int temp = lokacija % 4;
        switch (temp) {
            case 0:
                listaKombinacija.get(lokacija / 4).setA1(null);
                listaKombinacija.get(lokacija / 4).setSlika1(postaviSliku("8"));
                break;
            case 1:
                listaKombinacija.get(lokacija / 4).setA2(null);
                listaKombinacija.get(lokacija / 4).setSlika2(postaviSliku("8"));
                break;
            case 2:
                listaKombinacija.get(lokacija / 4).setA3(null);
                listaKombinacija.get(lokacija / 4).setSlika3(postaviSliku("8"));
                break;
            case 3:
                listaKombinacija.get(lokacija / 4).setA4(null);
                listaKombinacija.get(lokacija / 4).setSlika4(postaviSliku("8"));
                break;
        }
    }

    public void predajKombinaciju() {
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        uslov = false;
        int temp = lokacija / 4 - 1;
        String rez = listaKombinacija.get(temp).a1 + " ";
        rez += listaKombinacija.get(temp).a2;
        rez += " " + listaKombinacija.get(temp).a3;
        rez += " " + listaKombinacija.get(temp).a4;
        if (rez.compareTo(konacnaKombinacija) == 0) {

            dodatniUslov = false;
            gotovo = true;
            listaRezultata.get(lokacijaRezultata % 7).setA1("+");
            listaRezultata.get(lokacijaRezultata % 7).setA2("+");
            listaRezultata.get(lokacijaRezultata % 7).setA3("+");
            listaRezultata.get(lokacijaRezultata % 7).setA4("+");
            listaRezultata.get(lokacijaRezultata % 7).setSlika1(postaviSlikuResenja("0"));
            listaRezultata.get(lokacijaRezultata % 7).setSlika2(postaviSlikuResenja("0"));
            listaRezultata.get(lokacijaRezultata % 7).setSlika3(postaviSlikuResenja("0"));
            listaRezultata.get(lokacijaRezultata % 7).setSlika4(postaviSlikuResenja("0"));
            String rezOdigran = lokacijaRezultata + " " + "+ " + "+ " + "+ " + "+";
            lokacijaRezultata++;
            int osvojeniPoeni = 10;

            Odigranipotezi odigrao = new Odigranipotezi();
            odigrao.setIdKorisnika(reg.getId());
            odigrao.setIdPartije(partija.getId());
            odigrao.setTipIgre(new Byte("3"));
            odigrao.setStaJeUradjeno(rezOdigran + " " + rez);
            session.beginTransaction();
            session.save(odigrao);
            session.getTransaction().commit();

            Odigranipotezi odigrani = new Odigranipotezi();
            odigrani.setIdKorisnika(reg.getId());
            odigrani.setIdPartije(partija.getId());
            odigrani.setTipIgre(new Byte("-1"));
            odigrani.setStaJeUradjeno(String.valueOf(osvojeniPoeni));
            session.beginTransaction();
            session.save(odigrani);
            session.getTransaction().commit();
            if (redniBrPartije == 1) {
                if (partija.getIdKorisnika1() == reg.getId()) {
                    uslov = true;
                }
            } else {
                partija.setPotez(new Byte("1"));
                session.beginTransaction();
                session.update(partija);
                session.getTransaction().commit();
                gotovaIgra = true;
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("gamePage.xhtml");
                } catch (IOException ex) {
                    Logger.getLogger(SkockoKontroler.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            String[] niz = konacnaKombinacija.split(" ");
            ArrayList<String> t = new ArrayList<>();
            for (String s : niz) {
                t.add(s);
            }
            int sadrzi = 0;
            int tacno = 0;
            Kombinacija komb = new Kombinacija();
            komb.a1 = listaKombinacija.get(temp).a1;
            komb.a2 = listaKombinacija.get(temp).a2;
            komb.a3 = listaKombinacija.get(temp).a3;
            komb.a4 = listaKombinacija.get(temp).a4;
            if (listaKombinacija.get(temp).a1.compareTo(niz[0]) == 0) {
                tacno++;
                t.remove(listaKombinacija.get(temp).a1);
                listaKombinacija.get(temp).a1 = "-1";
            }
            if (listaKombinacija.get(temp).a2.compareTo(niz[1]) == 0) {
                tacno++;
                t.remove(listaKombinacija.get(temp).a2);
                listaKombinacija.get(temp).a2 = "-1";
            }
            if (listaKombinacija.get(temp).a3.compareTo(niz[2]) == 0) {
                tacno++;
                t.remove(listaKombinacija.get(temp).a3);
                listaKombinacija.get(temp).a3 = "-1";
            }
            if (listaKombinacija.get(temp).a4.compareTo(niz[3]) == 0) {
                tacno++;
                t.remove(listaKombinacija.get(temp).a4);
                listaKombinacija.get(temp).a4 = "-1";
            }
            if (t.contains(listaKombinacija.get(temp).a1)) {
                sadrzi++;
                t.remove(listaKombinacija.get(temp).a1);
            }
            if (t.contains(listaKombinacija.get(temp).a2)) {
                sadrzi++;
                t.remove(listaKombinacija.get(temp).a2);
            }
            if (t.contains(listaKombinacija.get(temp).a3)) {
                sadrzi++;
                t.remove(listaKombinacija.get(temp).a3);
            }
            if (t.contains(listaKombinacija.get(temp).a4)) {
                sadrzi++;
                t.remove(listaKombinacija.get(temp).a4);
            }
            for (int i = 0; i < tacno; i++) {
                if (i == 0) {
                    listaRezultata.get(lokacijaRezultata % 7).setA1("+");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika1(postaviSlikuResenja("0"));
                }
                if (i == 1) {
                    listaRezultata.get(lokacijaRezultata % 7).setA2("+");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika2(postaviSlikuResenja("0"));
                }
                if (i == 2) {
                    listaRezultata.get(lokacijaRezultata % 7).setA3("+");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika3(postaviSlikuResenja("0"));
                }
                if (i == 3) {
                    listaRezultata.get(lokacijaRezultata % 7).setA4("+");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika4(postaviSlikuResenja("0"));
                }
            }
            listaKombinacija.get(temp).a1 = komb.a1;
            listaKombinacija.get(temp).a2 = komb.a2;
            listaKombinacija.get(temp).a3 = komb.a3;
            listaKombinacija.get(temp).a4 = komb.a4;
            int sadrzi2 = sadrzi;
            for (int i = tacno; (i < 4) && (sadrzi != 0); i++) {
                if (i == 0) {
                    listaRezultata.get(lokacijaRezultata % 7).setA1("/");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika1(postaviSlikuResenja("1"));
                }
                if (i == 1) {
                    listaRezultata.get(lokacijaRezultata % 7).setA2("/");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika2(postaviSlikuResenja("1"));
                }
                if (i == 2) {
                    listaRezultata.get(lokacijaRezultata % 7).setA3("/");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika3(postaviSlikuResenja("1"));
                }
                if (i == 3) {
                    listaRezultata.get(lokacijaRezultata % 7).setA4("/");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika4(postaviSlikuResenja("1"));
                }
                sadrzi--;
            }
            for (int i = tacno + sadrzi2; i < 4; i++) {
                if (i == 0) {
                    listaRezultata.get(lokacijaRezultata % 7).setA1("-");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika1(postaviSlikuResenja("2"));
                }
                if (i == 1) {
                    listaRezultata.get(lokacijaRezultata % 7).setA2("-");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika2(postaviSlikuResenja("2"));
                }
                if (i == 2) {
                    listaRezultata.get(lokacijaRezultata % 7).setA3("-");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika3(postaviSlikuResenja("2"));
                }
                if (i == 3) {
                    listaRezultata.get(lokacijaRezultata % 7).setA4("-");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika4(postaviSlikuResenja("2"));
                }
            }
            String rezOdigran = listaRezultata.get(lokacijaRezultata % 7).getA1() + " " + listaRezultata.get(lokacijaRezultata % 7).getA2()
                    + " " + listaRezultata.get(lokacijaRezultata % 7).getA3() + " " + listaRezultata.get(lokacijaRezultata % 7).getA4();
            Odigranipotezi odigrao = new Odigranipotezi();
            odigrao.setIdKorisnika(reg.getId());
            odigrao.setIdPartije(partija.getId());
            odigrao.setTipIgre(new Byte("3"));
            odigrao.setStaJeUradjeno(lokacijaRezultata + " " + rezOdigran + " " + rez);

            session.beginTransaction();
            session.save(odigrao);
            session.getTransaction().commit();

            lokacijaRezultata++;
            if (lokacija == 24) {
                gotovo = true;
                sledeciSimbol(niz[0]);
                sledeciSimbol(niz[1]);
                sledeciSimbol(niz[2]);
                sledeciSimbol(niz[3]);
                uslov = false;
                dodatniUslov = false;
            }
        }
        session.close();
    }

    private String postaviSliku(String x) {
        switch (Integer.parseInt(x)) {
            case 0:
                return "/resources/images/skocko.png";
            case 1:
                return "/resources/images/zvezda.png";
            case 2:
                return "/resources/images/tref.png";
            case 3:
                return "/resources/images/karo.png";
            case 4:
                return "/resources/images/srce.png";
            case 5:
                return "/resources/images/pik.png";
        }
        return "";
    }

    private String postaviSlikuResenja(String x) {
        switch (Integer.parseInt(x)) {
            case 0:
                return "/resources/images/tacno.png";
            case 1:
                return "/resources/images/mozda.png";
            case 2:
                return "/resources/images/netacno.png";
        }
        return "";
    }

    public class Kombinacija {

        private String a1, a2, a3, a4;
        private String slika1, slika2, slika3, slika4;

        public String getSlika1() {
            return slika1;
        }

        public void setSlika1(String slika1) {
            this.slika1 = slika1;
        }

        public String getSlika2() {
            return slika2;
        }

        public void setSlika2(String slika2) {
            this.slika2 = slika2;
        }

        public String getSlika3() {
            return slika3;
        }

        public void setSlika3(String slika3) {
            this.slika3 = slika3;
        }

        public String getSlika4() {
            return slika4;
        }

        public void setSlika4(String slika4) {
            this.slika4 = slika4;
        }

        public String getA1() {
            return a1;
        }

        public void setA1(String a1) {
            this.a1 = a1;
        }

        public String getA2() {
            return a2;
        }

        public void setA2(String a2) {
            this.a2 = a2;
        }

        public String getA3() {
            return a3;
        }

        public void setA3(String a3) {
            this.a3 = a3;
        }

        public String getA4() {
            return a4;
        }

        public void setA4(String a4) {
            this.a4 = a4;
        }

    }

    public boolean isCekaj() {
        return cekaj;
    }

    public void setCekaj(boolean cekaj) {
        this.cekaj = cekaj;
    }

    public boolean isUslov() {
        return uslov;
    }

    public boolean isGotovaIgra() {
        return gotovaIgra;
    }

    public ArrayList<Kombinacija> getListaKombinacija() {
        return listaKombinacija;
    }

    public void setListaKombinacija(ArrayList<Kombinacija> listaKombinacija) {
        this.listaKombinacija = listaKombinacija;
    }

    public ArrayList<Kombinacija> getListaRezultata() {
        return listaRezultata;
    }

    public void setListaRezultata(ArrayList<Kombinacija> listaRezultata) {
        this.listaRezultata = listaRezultata;
    }

    public void setGotovaIgra(boolean gotovaIgra) {
        this.gotovaIgra = gotovaIgra;
    }

    public void setUslov(boolean uslov) {
        this.uslov = uslov;
    }

    public String getPoeni1() {
        return poeni1;
    }

    public void setPoeni1(String poeni1) {
        this.poeni1 = poeni1;
    }

    public String getPoeni2() {
        return poeni2;
    }

    public void setPoeni2(String poeni2) {
        this.poeni2 = poeni2;
    }

    public int getVreme() {
        return vreme;
    }

    public void setVreme(int vreme) {
        this.vreme = vreme;
    }

}
