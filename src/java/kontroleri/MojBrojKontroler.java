/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kontroleri;

import db.HibernateUtil;
import entiteti.Odigranipotezi;
import entiteti.Partije;
import entiteti.Registracija;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@SessionScoped
public class MojBrojKontroler {

    private boolean uslov = false;
    private int lokacija = 0;
    private String izraz, izraz2, poeni1, poeni2;
    private int broj0, broj1, broj2, broj3, dvocifren, trocifren, rezultat;
    private boolean dodatniUslov = false;
    private int vreme = 60;
    private boolean gotovo = false, gotovaIgra = false;
    private int redniBrPartije = 0;
    private Registracija reg;
    private Partije partija = null;

    public void init() {
        if (gotovo == true) {
            gotovo = false;
            dodatniUslov = false;
            vreme = 60;
            broj0 = 0;
            broj1 = 0;
            broj2 = 0;
            broj3 = 0;
            dvocifren = 0;
            trocifren = 0;
            rezultat = 0;
            izraz = "";
            izraz2 = "";
        }
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession s = (HttpSession) fc.getExternalContext().getSession(false);
        reg = (Registracija) s.getAttribute("user");
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Partije.class);
        partija = (Partije) query.add(Restrictions.eq("idKorisnika1", reg.getId())).add(Restrictions.eq("status", new Byte("1"))).uniqueResult();
        session.getTransaction().commit();
        if (partija == null) {
            query = session.createCriteria(Partije.class);
            session.beginTransaction();
            partija = (Partije) query.add(Restrictions.eq("idKorisnika2", reg.getId())).add(Restrictions.eq("status", new Byte("1"))).uniqueResult();
            session.getTransaction().commit();
        }
        session.close();
        if (partija == null) {
            poeni1 = "0";
            poeni2 = "0";
            uslov = true;
        } else {
            if (partija.getPotez() != new Byte("2")) {
                if (partija.getIdKorisnika1() != reg.getId()) {
                    uslov = true;
                }
            }
            poeni1 = String.valueOf(partija.getPoeni1());
            poeni2 = String.valueOf(partija.getPoeni2());
        }
    }

    public void sledeciBroj() {
        switch (lokacija) {
            case 0:
                broj0 = 1 + new Random(System.currentTimeMillis()).nextInt(9);
                break;
            case 1:
                broj1 = 1 + new Random(System.currentTimeMillis()).nextInt(9);
                break;
            case 2:
                broj2 = 1 + new Random(System.currentTimeMillis()).nextInt(9);
                break;
            case 3:
                broj3 = 1 + new Random(System.currentTimeMillis()).nextInt(9);
                break;
            case 4:
                dvocifren = 5 * (2 + new Random(System.currentTimeMillis()).nextInt(3));
                break;
            case 5:
                trocifren = 25 * (1 + new Random(System.currentTimeMillis()).nextInt(3));
                break;
            case 6:
                rezultat = 1 + new Random(System.currentTimeMillis()).nextInt(999);
                break;

        }
        if (rezultat != 0) {
            ((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false)).setAttribute("rezultat", rezultat);
        }
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        Odigranipotezi odrigrano = new Odigranipotezi();
        odrigrano.setIdPartije(partija.getId());
        odrigrano.setIdKorisnika(reg.getId());
        odrigrano.setTipIgre(new Byte("2"));
        odrigrano.setStaJeUradjeno(broj0 + " " + broj1 + " " + broj2 + " "
                + broj3 + " " + dvocifren + " " + trocifren + " " + rezultat);
        session.beginTransaction();
        session.save(odrigrano);
        session.getTransaction().commit();
        session.close();
        lokacija++;
        if (lokacija == 7) {
            uslov = true;
            dodatniUslov = true;
            lokacija = 0;
        }
    }

    public void predajIzraz() {
        dodatniUslov = false;
        gotovo = true;
        uslov = false;

        ArrayList<String> brojevi = new ArrayList<>();
        brojevi.add("" + broj0);
        brojevi.add("" + broj1);
        brojevi.add("" + broj2);
        brojevi.add("" + broj3);
        brojevi.add("" + dvocifren);
        brojevi.add("" + trocifren);
        ArrayList<String> znakovi = new ArrayList<>();
        znakovi.add("+");
        znakovi.add("-");
        znakovi.add("*");
        znakovi.add("/");
        znakovi.add("(");
        znakovi.add(")");
        ArrayList<String> cifre = new ArrayList<>();
        cifre.add("0");
        cifre.add("1");
        cifre.add("2");
        cifre.add("3");
        cifre.add("4");
        cifre.add("5");
        cifre.add("6");
        cifre.add("7");
        cifre.add("8");
        cifre.add("9");

        boolean validnost = true;
        for (int i = 0; i < izraz.length(); i++) {
            String temp = "" + izraz.charAt(i);
            if (!znakovi.contains(temp)) {
                if (i + 1 < izraz.length()) {
                    if (cifre.contains("" + izraz.charAt(i + 1))) {
                        i++;
                        temp += izraz.charAt(i);
                    }
                }
                if (i + 1 < izraz.length()) {
                    if (cifre.contains("" + izraz.charAt(i + 1))) {
                        i++;
                        temp += izraz.charAt(i);
                    }
                }
            }
            if (brojevi.contains(temp)) {
                brojevi.remove(temp);
            } else {
                if (!znakovi.contains(temp)) {
                    validnost = false;
                    break;
                }
            }
        }
        int osvojeniPoeni = -1;
        if (validnost) {
            try {
                osvojeniPoeni = (int) new ScriptEngineManager().getEngineByName("JavaScript").eval(izraz);
            } catch (ScriptException ex) {
                Logger.getLogger(MojBrojKontroler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (Integer.parseInt(izraz2) != osvojeniPoeni) {
            osvojeniPoeni = -1;
        }

        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        Odigranipotezi odigrani = new Odigranipotezi();
        odigrani.setIdKorisnika(reg.getId());
        odigrani.setIdPartije(partija.getId());
        odigrani.setTipIgre(new Byte("0"));
        odigrani.setStaJeUradjeno(String.valueOf(osvojeniPoeni));
        session.beginTransaction();
        session.save(odigrani);
        session.getTransaction().commit();

        if (redniBrPartije == 0) {
            redniBrPartije++;
            if (partija.getIdKorisnika1() == reg.getId()) {
                uslov = true;
            }
            if (partija.getPotez() != new Byte("2")) {
                partija.setPotez(new Byte("2"));
                session.beginTransaction();
                session.update(partija);
                session.getTransaction().commit();
            }
            Criteria query = session.createCriteria(Odigranipotezi.class);
            session.beginTransaction();
            ArrayList<Odigranipotezi> potez = (ArrayList<Odigranipotezi>) query.add(Restrictions.eq("idPartije", partija.getId()))
                    .add(Restrictions.eq("tipIgre", new Byte("2"))).list();
            session.getTransaction().commit();
            for (int i = 0; i < potez.size(); i++) {
                session.beginTransaction();
                Odigranipotezi pote = (Odigranipotezi) session.merge(potez.get(i));
                session.delete(pote);
                session.getTransaction().commit();
            }
            init();
        } else {
            proveravaj();
            partija.setPotez(new Byte("1"));
            session.beginTransaction();
            session.update(partija);
            session.getTransaction().commit();
            gotovaIgra = true;
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("skocko.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(slagalicaKontroler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        session.close();

    }

    public void promenaVremena() {
        if (dodatniUslov) {
            if (vreme >= 0) {
                vreme--;
                if (vreme == 0) {
                    if (!gotovo) {
                        predajIzraz();
                    }
                }
            }
        }
    }

    public void proveravaj() {
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        if (partija == null) {
            session.beginTransaction();
            Criteria query = session.createCriteria(Partije.class);
            partija = (Partije) query.add(Restrictions.eq("idKorisnika1", reg.getId())).add(Restrictions.eq("status", new Byte("1"))).uniqueResult();
            session.getTransaction().commit();
            if (partija == null) {
                query = session.createCriteria(Partije.class);
                session.beginTransaction();
                partija = (Partije) query.add(Restrictions.eq("idKorisnika2", reg.getId())).add(Restrictions.eq("status", new Byte("1"))).uniqueResult();
                session.getTransaction().commit();
            }
        }
        Criteria query = session.createCriteria(Odigranipotezi.class);
        session.beginTransaction();
        ArrayList<Odigranipotezi> potez1 = (ArrayList<Odigranipotezi>) query.add(Restrictions.eq("idPartije", partija.getId()))
                .add(Restrictions.eq("tipIgre", new Byte("0"))).list();
        session.getTransaction().commit();
        if (potez1.size() == 2) {
            Odigranipotezi ewr = potez1.get(0);
            String ghj = potez1.get(0).getStaJeUradjeno();
            int poen1 = Integer.parseInt(potez1.get(0).getStaJeUradjeno());
            int poen2 = Integer.parseInt(potez1.get(1).getStaJeUradjeno());
            rezultat = (int) ((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false)).getAttribute("rezultat");
            if ((poen1 == -1) || (poen2 == -1)) {
                if ((poen1 == -1) && (poen2 == -1)) {
                    poen1 = 0;
                    poen2 = 0;
                } else {
                    if (poen1 == -1) {
                        poen1 = 0;
                        poen2 = 10;
                    }
                    if (poen2 == -1) {
                        poen2 = 0;
                        poen1 = 10;
                    }
                }
            } else {
                if (Math.abs(rezultat - poen1) < Math.abs(rezultat - poen2)) {
                    poen1 = 10;
                    poen2 = 0;
                } else if (Math.abs(rezultat - poen1) > Math.abs(rezultat - poen2)) {
                    poen2 = 10;
                    poen1 = 0;
                } else {
                    poen1 = 5;
                    poen2 = 5;
                }
            }
            if (poen1 > 10) {
                poen1 = 0;
            }
            if (poen2 > 10) {
                poen2 = 0;
            }
            if (potez1.get(0).getIdKorisnika() == partija.getIdKorisnika1()) {
                partija.setPoeni1(partija.getPoeni1() + poen1);
                partija.setPoeni2(partija.getPoeni2() + poen2);
            } else {
                partija.setPoeni1(poen2 + partija.getPoeni1());
                partija.setPoeni2(poen1 + partija.getPoeni2());
            }
            session.beginTransaction();
            session.update(partija);
            session.getTransaction().commit();
            session.beginTransaction();
            Odigranipotezi pote = (Odigranipotezi) session.merge(potez1.get(0));
            session.delete(pote);
            session.getTransaction().commit();
            session.beginTransaction();
            pote = (Odigranipotezi) session.merge(potez1.get(1));
            session.delete(pote);
            session.getTransaction().commit();
        }
        boolean radi = false;
        poeni1 = String.valueOf(partija.getPoeni1());
        poeni2 = String.valueOf(partija.getPoeni2());
        if (partija.getPotez() == new Byte("1")) {
            if (partija.getIdKorisnika1() != reg.getId()) {
                radi = true;
            }
        } else {
            if (partija.getIdKorisnika2() != reg.getId()) {
                radi = true;
            }
        }
        if (radi) {
            query = session.createCriteria(Odigranipotezi.class);
            session.beginTransaction();
            ArrayList<Odigranipotezi> potez = (ArrayList<Odigranipotezi>) query.add(Restrictions.eq("idPartije", partija.getId()))
                    .add(Restrictions.eq("tipIgre", new Byte("2"))).list();
            session.getTransaction().commit();
            for (int i = 0; i < potez.size(); i++) {
                String[] delovi = potez.get(i).getStaJeUradjeno().split(" ");
                broj0 = Integer.parseInt(delovi[0]);
                broj1 = Integer.parseInt(delovi[1]);
                broj2 = Integer.parseInt(delovi[2]);
                broj3 = Integer.parseInt(delovi[3]);
                dvocifren = Integer.parseInt(delovi[4]);
                trocifren = Integer.parseInt(delovi[5]);
                rezultat = Integer.parseInt(delovi[6]);
                if (rezultat != 0) {
                    lokacija = -1;
                    dodatniUslov = true;
                }
                session.beginTransaction();
                Odigranipotezi pote = (Odigranipotezi) session.merge(potez.get(i));
                session.delete(pote);
                session.getTransaction().commit();
            }
        }
        session.close();
    }

    public int getVreme() {
        return vreme;
    }

    public void setVreme(int vreme) {
        this.vreme = vreme;
    }

    public boolean isUslov() {
        return uslov;
    }

    public void setUslov(boolean uslov) {
        this.uslov = uslov;
    }

    public String getIzraz() {
        return izraz;
    }

    public void setIzraz(String izraz) {
        this.izraz = izraz;
    }

    public String getPoeni1() {
        return poeni1;
    }

    public String getIzraz2() {
        return izraz2;
    }

    public void setIzraz2(String izraz2) {
        this.izraz2 = izraz2;
    }

    public void setPoeni1(String poeni1) {
        this.poeni1 = poeni1;
    }

    public boolean isGotovaIgra() {
        return gotovaIgra;
    }

    public void setGotovaIgra(boolean gotovaIgra) {
        this.gotovaIgra = gotovaIgra;
    }

    public String getPoeni2() {
        return poeni2;
    }

    public void setPoeni2(String poeni2) {
        this.poeni2 = poeni2;
    }

    public int getBroj0() {
        return broj0;
    }

    public void setBroj0(int broj0) {
        this.broj0 = broj0;
    }

    public int getBroj1() {
        return broj1;
    }

    public void setBroj1(int broj1) {
        this.broj1 = broj1;
    }

    public int getBroj2() {
        return broj2;
    }

    public void setBroj2(int broj2) {
        this.broj2 = broj2;
    }

    public int getBroj3() {
        return broj3;
    }

    public void setBroj3(int broj3) {
        this.broj3 = broj3;
    }

    public int getDvocifren() {
        return dvocifren;
    }

    public void setDvocifren(int dvocifren) {
        this.dvocifren = dvocifren;
    }

    public int getTrocifren() {
        return trocifren;
    }

    public void setTrocifren(int trocifren) {
        this.trocifren = trocifren;
    }

    public int getRezultat() {
        return rezultat;
    }

    public void setRezultat(int rezultat) {
        this.rezultat = rezultat;
    }

}
