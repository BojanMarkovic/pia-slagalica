/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kontroleri;

import db.HibernateUtil;
import entiteti.Partije;
import entiteti.Registracija;
import java.io.IOException;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@RequestScoped
public class LoginKontroler implements Serializable {

    private String korisnickoIme = "";
    private String lozinka;
    private String poruka;

    public String getPoruka() {
        return poruka;
    }

    public void setPoruka(String poruka) {
        this.poruka = poruka;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public void logout() {
        try {
            FacesContext fc = FacesContext.getCurrentInstance();
            HttpSession s = (HttpSession) fc.getExternalContext().getSession(false);
            Registracija reg = (Registracija) s.getAttribute("user");
            if (reg != null) {
                SessionFactory sessionF = HibernateUtil.getSessionFactory();
                Session session = sessionF.openSession();
                session.beginTransaction();
                Criteria query = session.createCriteria(Partije.class);
                Partije partija = (Partije) query.add(Restrictions.eq("idKorisnika1", reg.getId())).add(Restrictions.eq("status", new Byte("1"))).uniqueResult();
                session.getTransaction().commit();
                if (partija == null) {
                    query = session.createCriteria(Partije.class);
                    session.beginTransaction();
                    partija = (Partije) query.add(Restrictions.eq("idKorisnika2", reg.getId())).add(Restrictions.eq("status", new Byte("1"))).uniqueResult();
                    session.getTransaction().commit();
                }
                if (partija != null) {
                    partija.setStatus(new Byte("2"));
                    session.beginTransaction();
                    session.update(partija);
                    session.getTransaction().commit();
                }
                session.close();
            }
            s.setAttribute("user", null);
            s.invalidate();
            FacesContext.getCurrentInstance().getExternalContext().redirect("PocetnaStrana.xhtml?i=0");
        } catch (IOException ex) {
            Logger.getLogger(LoginKontroler.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void login() {
        try {
            SessionFactory sessionF = HibernateUtil.getSessionFactory();
            Session session = sessionF.openSession();
            session.beginTransaction();

            Criteria query = session.createCriteria(Registracija.class);
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(lozinka.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            lozinka = sb.toString();
            Registracija user = (Registracija) query.add(Restrictions.eq("korisnickoIme", korisnickoIme)).add(Restrictions.eq("lozinka", lozinka)).uniqueResult();
            session.getTransaction().commit();
            session.close();
            if (user == null) {
                poruka = "Unete su loše informacije!";
            }
            if (user != null) {
                FacesContext fc = FacesContext.getCurrentInstance();
                HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
                hs.setAttribute("user", user);
                poruka = "Uspešno ulogovan";
                try {
                    System.out.println(user.getTip());
                    if (user.getTip() == new Byte("1")) {
                        FacesContext.getCurrentInstance().getExternalContext().redirect("adminPage.xhtml?i=3");
                    }
                    if (user.getTip() == new Byte("2")) {
                        FacesContext.getCurrentInstance().getExternalContext().redirect("supervizorPage.xhtml?i=4");
                    }
                    if (user.getTip() == new Byte("3")) {
                        FacesContext.getCurrentInstance().getExternalContext().redirect("gamePage.xhtml?i=2");
                    }
                    if (user.getTip() == new Byte("4")) {
                        poruka = "Nije vam potvrđena registracija!";
                        return;
                    }
                } catch (IOException ex) {
                    Logger.getLogger(LoginKontroler.class.getName()).log(Level.SEVERE, null, ex);
                }
                FacesContext.getCurrentInstance().responseComplete();
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(LoginKontroler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
