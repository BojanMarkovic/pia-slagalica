/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kontroleri;

import db.HibernateUtil;
import entiteti.Definisanjeigradana;
import entiteti.Igraasocijacije;
import entiteti.Igradana;
import entiteti.Igraspojnice;
import entiteti.Registracija;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.jdbc.internal.BinaryStreamImpl;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

@ManagedBean
@SessionScoped
public class AdminKontroler {

    private ArrayList<Registracija> korisnici;
    private ArrayList<String> listaSpojnica, listaAsocijacija;
    private Date datum;
    private String spojnice;
    private String asocijacije;
    private String poruka = "";
    private StreamedContent slika;
    private String slagalica;
    private String mojBroj;
    private String skocko;

    public void dodajIgruDana() {
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Definisanjeigradana.class);
        ArrayList<Definisanjeigradana> temp = (ArrayList<Definisanjeigradana>) query.list();
        session.getTransaction().commit();
        session.beginTransaction();
        query = session.createCriteria(Igradana.class);
        ArrayList<Igradana> tempIgre = (ArrayList<Igradana>) query.list();
        session.getTransaction().commit();
        boolean uslov2 = true;
        for (Definisanjeigradana d : temp) {
            if (d.getDatum().getTime() == datum.getTime()) {
                uslov2 = false;
                boolean uslov = true;
                for (Igradana t : tempIgre) {
                    if (t.getDatum().getTime() == datum.getTime()) {
                        uslov = false;
                        break;
                    }
                }
                if (uslov) {
                    session.beginTransaction();
                    d.setIdAsocijacije(Integer.parseInt(asocijacije));
                    d.setVrstaSpojnica(Integer.parseInt(spojnice));
                    d.setSeedMojBroj(Integer.parseInt(mojBroj));
                    d.setSeedSkocko(Integer.parseInt(skocko));
                    d.setSeedSlagalica(Integer.parseInt(slagalica));
                    session.update(d);
                    session.getTransaction().commit();
                    poruka = "Uspešno dodata igra dana!";
                } else {
                    poruka = "nije  moguće dodati igru dana!";
                    break;
                }
            }
        }
        if (uslov2) {
            session.beginTransaction();
            Definisanjeigradana def = new Definisanjeigradana();
            def.setDatum(datum);
            def.setIdAsocijacije(Integer.parseInt(asocijacije));
            def.setVrstaSpojnica(Integer.parseInt(spojnice));
            def.setSeedMojBroj(Integer.parseInt(mojBroj));
            def.setSeedSkocko(Integer.parseInt(skocko));
            def.setSeedSlagalica(Integer.parseInt(slagalica));
            session.save(def);
            session.getTransaction().commit();
            poruka = "Uspešno dodata igra dana!";
        }
        session.close();
    }

    public AdminKontroler() {
        listaAsocijacija = new ArrayList<>();
        listaSpojnica = new ArrayList<>();
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Registracija.class);
        korisnici = (ArrayList<Registracija>) query.add(Restrictions.eq("tip", new Byte("4"))).list();
        session.getTransaction().commit();
        session.beginTransaction();
        query = session.createCriteria(Igraspojnice.class);
        ArrayList<Igraspojnice> temp = (ArrayList<Igraspojnice>) query.list();
        session.getTransaction().commit();
        for (Igraspojnice t : temp) {
            if (!listaSpojnica.contains(String.valueOf(t.getVrsta()))) {
                listaSpojnica.add(String.valueOf(t.getVrsta()));
            }
        }
        session.beginTransaction();
        query = session.createCriteria(Igraasocijacije.class);
        ArrayList<Igraasocijacije> temp2 = (ArrayList<Igraasocijacije>) query.list();
        session.getTransaction().commit();
        for (Igraasocijacije t : temp2) {
            if (!listaAsocijacija.contains(String.valueOf(t.getId()))) {
                listaAsocijacija.add(String.valueOf(t.getId()));
            }
        }
        session.close();
    }

    public void potvrdi(Registracija r) {
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Registracija.class);
        Registracija reg = (Registracija) query.add(Restrictions.eq("id", r.getId())).uniqueResult();
        session.getTransaction().commit();
        if (reg != null) {
            reg.setTip(new Byte("3"));
            session.beginTransaction();
            session.update(reg);
            session.getTransaction().commit();
            session.beginTransaction();
            query = session.createCriteria(Registracija.class);
            korisnici = (ArrayList<Registracija>) query.add(Restrictions.eq("tip", new Byte("4"))).list();
            session.getTransaction().commit();
        }
        session.close();
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("adminPage.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(AdminKontroler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void odbij(Registracija r) {
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        r = (Registracija) session.merge(r);
        session.delete(r);
        session.getTransaction().commit();
        session.beginTransaction();
        Criteria query = session.createCriteria(Registracija.class);
        korisnici = (ArrayList<Registracija>) query.add(Restrictions.eq("tip", new Byte("4"))).list();
        session.getTransaction().commit();
        session.close();
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("adminPage.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(AdminKontroler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getSlagalica() {
        return slagalica;
    }

    public void setSlagalica(String slagalica) {
        this.slagalica = slagalica;
    }

    public String getMojBroj() {
        return mojBroj;
    }

    public void setMojBroj(String mojBroj) {
        this.mojBroj = mojBroj;
    }

    public String getSkocko() {
        return skocko;
    }

    public void setSkocko(String skocko) {
        this.skocko = skocko;
    }

    public String getPoruka() {
        return poruka;
    }

    public void setPoruka(String poruka) {
        this.poruka = poruka;
    }

    public ArrayList<String> getListaSpojnica() {
        return listaSpojnica;
    }

    public void setListaSpojnica(ArrayList<String> listaSpojnica) {
        this.listaSpojnica = listaSpojnica;
    }

    public ArrayList<String> getListaAsocijacija() {
        return listaAsocijacija;
    }

    public void setListaAsocijacija(ArrayList<String> listaAsocijacija) {
        this.listaAsocijacija = listaAsocijacija;
    }

    public String getSpojnice() {
        return spojnice;
    }

    public void setSpojnice(String spojnice) {
        this.spojnice = spojnice;
    }

    public String getAsocijacije() {
        return asocijacije;
    }

    public void setAsocijacije(String asocijacije) {
        this.asocijacije = asocijacije;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public ArrayList<Registracija> getKorisnici() {
        return korisnici;
    }

    public void setKorisnici(ArrayList<Registracija> korisnici) {
        this.korisnici = korisnici;
    }

    public StreamedContent getSlika() {
        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        } else {
            int id = Integer.parseInt(context.getExternalContext().getRequestParameterMap().get("ID"));

            SessionFactory sessionF = HibernateUtil.getSessionFactory();
            Session session = sessionF.openSession();
            session.beginTransaction();

            Criteria query = session.createCriteria(Registracija.class);
            Registracija kor = (Registracija) query.add(Restrictions.eq("id", id)).uniqueResult();
            session.getTransaction().commit();
            session.close();

            if (kor == null) {
                return null;
            }
            return new DefaultStreamedContent(new BinaryStreamImpl(kor.getSlika()), "image/png");

        }
    }

    public void setSlika(StreamedContent slika) {
        this.slika = slika;
    }

}
