/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kontroleri;

import db.HibernateUtil;
import entiteti.Cekanje;
import entiteti.Definisanjeigradana;
import entiteti.Igradana;
import entiteti.Partije;
import entiteti.Registracija;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.axes.cartesian.CartesianScales;
import org.primefaces.model.charts.axes.cartesian.linear.CartesianLinearAxes;
import org.primefaces.model.charts.axes.cartesian.linear.CartesianLinearTicks;
import org.primefaces.model.charts.bar.BarChartDataSet;
import org.primefaces.model.charts.bar.BarChartModel;
import org.primefaces.model.charts.bar.BarChartOptions;
import org.primefaces.model.charts.optionconfig.legend.Legend;
import org.primefaces.model.charts.optionconfig.legend.LegendLabel;
import org.primefaces.model.charts.optionconfig.title.Title;

@ManagedBean
@SessionScoped
public class gameKontroler implements Serializable {

    private String poruka1, poruka2;
    private boolean cekaj = true;
    private Registracija reg;
    private ArrayList<topDanaKorisnici> listaDana;
    private ArrayList<PartijeMoje> mojePartije;
    private BarChartModel barModel;

    public void createBarChart() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession s = (HttpSession) fc.getExternalContext().getSession(false);
        Registracija reg = (Registracija) s.getAttribute("user");
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Igradana.class);
        query.addOrder(Order.desc("id"));
        Igradana igra = (Igradana) query.add(Restrictions.eq("idKorisnika", reg.getId())).setMaxResults(1).uniqueResult();
        session.getTransaction().commit();
        session.close();

        barModel = new BarChartModel();
        ChartData data = new ChartData();

        BarChartDataSet barDataSet = new BarChartDataSet();
        barDataSet.setLabel("Igra dana-broj poena");

        ArrayList<Number> values = new ArrayList<>();
        values.add(igra.getPoeniiSlagalica());
        values.add(igra.getPoeniMojBroj());
        values.add(igra.getPoeniSkocko());
        values.add(igra.getPoeniSpojnice());
        values.add(igra.getPoeniAsocijacije());
        barDataSet.setData(values);

        ArrayList<String> bgColor = new ArrayList<>();
        bgColor.add("rgba(255, 99, 132, 0.2)");
        bgColor.add("rgba(255, 159, 64, 0.2)");
        bgColor.add("rgba(255, 205, 86, 0.2)");
        bgColor.add("rgba(75, 192, 192, 0.2)");
        bgColor.add("rgba(54, 162, 235, 0.2)");
        barDataSet.setBackgroundColor(bgColor);

        ArrayList<String> borderColor = new ArrayList<>();
        borderColor.add("rgb(255, 99, 132)");
        borderColor.add("rgb(255, 159, 64)");
        borderColor.add("rgb(255, 205, 86)");
        borderColor.add("rgb(75, 192, 192)");
        borderColor.add("rgb(54, 162, 235)");
        barDataSet.setBorderColor(borderColor);
        barDataSet.setBorderWidth(1);

        data.addChartDataSet(barDataSet);

        ArrayList<String> labels = new ArrayList<>();
        labels.add("Slagalica");
        labels.add("Moj broj");
        labels.add("Skočko");
        labels.add("Spojnice");
        labels.add("Asocijacije");
        data.setLabels(labels);
        barModel.setData(data);

        //Options
        BarChartOptions options = new BarChartOptions();
        CartesianScales cScales = new CartesianScales();
        CartesianLinearAxes linearAxes = new CartesianLinearAxes();
        CartesianLinearTicks ticks = new CartesianLinearTicks();
        ticks.setBeginAtZero(true);
        linearAxes.setTicks(ticks);
        cScales.addYAxesData(linearAxes);
        options.setScales(cScales);

        Title title = new Title();
        title.setDisplay(true);
        title.setText("Igra dana");
        options.setTitle(title);

        Legend legend = new Legend();
        legend.setDisplay(true);
        legend.setPosition("top");
        LegendLabel legendLabels = new LegendLabel();
        legendLabels.setFontStyle("bold");
        legendLabels.setFontColor("#2980B9");
        legendLabels.setFontSize(24);
        legend.setLabels(legendLabels);
        options.setLegend(legend);

        barModel.setOptions(options);
    }

    public gameKontroler() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession s = (HttpSession) fc.getExternalContext().getSession(false);
        reg = (Registracija) s.getAttribute("user");
        listaDana = new ArrayList<>();
        topListaDana();
        mojeIgre();
    }

    private void mojeIgre() {
        poruka1 = "";
        poruka2 = "";
        mojePartije = new ArrayList<>();
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Partije.class);
        ArrayList<Partije> temp = (ArrayList<Partije>) query.add(Restrictions.eq("idKorisnika1", reg.getId())).list();
        session.getTransaction().commit();
        session.beginTransaction();
        query = session.createCriteria(Partije.class);
        temp.addAll((ArrayList<Partije>) query.add(Restrictions.eq("idKorisnika2", reg.getId())).list());
        session.getTransaction().commit();
        long DAY_IN_MS = 1000 * 60 * 60 * 24;
        Date datumPre10 = new Date(System.currentTimeMillis() - (10 * DAY_IN_MS));
        for (Partije p : temp) {
            if (p.getDatum().getTime() < datumPre10.getTime()) {
                temp.remove(p);
            }
        }
        for (Partije p : temp) {
            PartijeMoje x = new PartijeMoje();
            x.setDatum(p.getDatum());
            x.setIdPartije(p.getId());
            if (p.getIdKorisnika1() == reg.getId()) {
                x.setPoeniMoji(p.getPoeni1());
                x.setPoeniProtivnika(p.getPoeni2());
                query = session.createCriteria(Registracija.class);
                session.beginTransaction();
                Registracija kor = (Registracija) query.add(Restrictions.eq("id", p.getIdKorisnika2())).uniqueResult();
                session.getTransaction().commit();
                x.setImeProtivnika(kor.getKorisnickoIme());
                x.init(p.getPoeniiSlagalica1(), p.getPoeniMojBroj1(), p.getPoeniSkocko1(), p.getPoeniSpojnice1(), p.getPoeniAsocijacije1(),
                        p.getPoeniiSlagalica2(), p.getPoeniMojBroj2(), p.getPoeniSkocko2(), p.getPoeniSpojnice2(), p.getPoeniAsocijacije2());
            }
            if (p.getIdKorisnika2() == reg.getId()) {
                x.setPoeniMoji(p.getPoeni2());
                x.setPoeniProtivnika(p.getPoeni1());
                query = session.createCriteria(Registracija.class);
                session.beginTransaction();
                Registracija kor = (Registracija) query.add(Restrictions.eq("id", p.getIdKorisnika2())).uniqueResult();
                session.getTransaction().commit();
                x.setImeProtivnika(kor.getKorisnickoIme());
                x.init(p.getPoeniiSlagalica2(), p.getPoeniMojBroj2(), p.getPoeniSkocko2(), p.getPoeniSpojnice2(), p.getPoeniAsocijacije2(),
                        p.getPoeniiSlagalica1(), p.getPoeniMojBroj1(), p.getPoeniSkocko1(), p.getPoeniSpojnice1(), p.getPoeniAsocijacije1());
            }
            mojePartije.add(x);
        }
        session.close();
    }

    public void provera() {
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Cekanje.class);
        Cekanje user = (Cekanje) query.add(Restrictions.eq("idKorisnika", reg.getId())).uniqueResult();
        session.getTransaction().commit();
        if (user.getStatus() == new Byte("2")) {
            cekaj = false;
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("slagalica.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(gameKontroler.class.getName()).log(Level.SEVERE, null, ex);
            }
            query = session.createCriteria(Cekanje.class);
            user = (Cekanje) query.add(Restrictions.eq("idKorisnika", reg.getId())).uniqueResult();
            session.beginTransaction();
            Cekanje cekanje = (Cekanje) session.merge(user);
            session.delete(cekanje);
            session.getTransaction().commit();
            Partije partija = new Partije();
            partija.setDatum(new Date());
            partija.setIdKorisnika1(user.getIdKorisnika());
            partija.setIdKorisnika2(user.getIdKorisnika2());
            partija.setPoeni1(0);
            partija.setPoeni2(0);
            partija.setStatus(new Byte("1"));
            partija.setPotez(new Byte("1"));
            session.beginTransaction();
            session.save(partija);
            session.getTransaction().commit();
            session.close();
        }

    }

    public void izadjiIzSobe() {
        poruka1 = "";
        poruka2 = "";
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Cekanje.class);
        Cekanje user = (Cekanje) query.add(Restrictions.eq("idKorisnika", reg.getId())).uniqueResult();
        session.getTransaction().commit();
        Cekanje cekanje = new Cekanje();
        cekanje.setIdKorisnika(reg.getId());
        cekanje.setId(user.getId());
        cekanje.setStatus(user.getStatus());
        cekanje.setIdKorisnika2(user.getIdKorisnika2());
        session.beginTransaction();
        cekanje = (Cekanje) session.merge(cekanje);
        session.delete(cekanje);
        session.getTransaction().commit();
        session.close();
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("gamePage.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(gameKontroler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void udjiUCekanje() {
        poruka1 = "";
        poruka2 = "";
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        Cekanje cekanje = new Cekanje();
        cekanje.setIdKorisnika(reg.getId());
        cekanje.setStatus(new Byte("1"));
        session.beginTransaction();
        session.save(cekanje);
        session.getTransaction().commit();
        session.close();
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("sobaZaCekanje.xhtml");

        } catch (IOException ex) {
            Logger.getLogger(LoginKontroler.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void prikljuciSe() {
        poruka1 = "";
        poruka2 = "";
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Cekanje.class);
        ArrayList<Cekanje> temp = (ArrayList<Cekanje>) query.list();
        session.getTransaction().commit();
        if (temp.size() == 0) {
            poruka1 = "Nema igrača koji čekaju!";
        } else {
            for (int i = 0; i < temp.size(); i++) {
                if (temp.get(i).getStatus() == new Byte("1")) {
                    cekaj = false;
                    session.beginTransaction();
                    temp.get(i).setIdKorisnika2(reg.getId());
                    temp.get(i).setStatus(new Byte("2"));
                    session.update(temp.get(i));
                    session.getTransaction().commit();
                    try {
                        FacesContext.getCurrentInstance().getExternalContext().redirect("slagalica.xhtml");
                    } catch (IOException ex) {
                        Logger.getLogger(gameKontroler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    session.close();
                    return;
                }
            }
        }
        session.close();
    }

    public void igraDana() {
        poruka1 = "";
        poruka2 = "";
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Igradana.class);
        ArrayList<Igradana> temp = (ArrayList<Igradana>) query.list();
        session.getTransaction().commit();
        boolean uslov = true;
        for (Igradana i : temp) {
            if (i.getDatum().getDate() == new Date().getDate()) {
                if (i.getIdKorisnika() == reg.getId()) {
                    uslov = false;
                    break;
                }
            }
        }
        if (uslov) {
            session.beginTransaction();
            query = session.createCriteria(Definisanjeigradana.class);
            ArrayList<Definisanjeigradana> def = (ArrayList<Definisanjeigradana>) query.list();
            session.getTransaction().commit();
            boolean prov = true;
            for (Definisanjeigradana d : def) {
                if ((new Date().getYear() == d.getDatum().getYear())
                        && (new Date().getDay() == d.getDatum().getDay())
                        && (new Date().getMonth() == d.getDatum().getMonth())) {
                    prov = false;
                    break;
                }
            }
            if (prov) {
                poruka2 = "Igra dana nije definisana";
            } else {
                session.beginTransaction();
                Igradana igra = new Igradana();
                igra.setDatum(new Date());
                igra.setIdKorisnika(reg.getId());
                igra.setPoeni(0);
                session.save(igra);
                session.getTransaction().commit();
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("IgradanaSlagalica.xhtml");
                } catch (IOException ex) {
                    Logger.getLogger(gameKontroler.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            poruka2 = "Igra dana je završena!";
        }
        session.close();
    }

    private void topListaDana() {
        poruka1 = "";
        poruka2 = "";
        listaDana = new ArrayList<>();
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Igradana.class);
        ArrayList<Igradana> temp = (ArrayList<Igradana>) query.list();
        session.getTransaction().commit();
        int redniBr = 1;
        for (Igradana i : temp) {
            Date d = new Date();
            if ((i.getDatum().getYear() == d.getYear())
                    && (i.getDatum().getDay() == d.getDay())
                    && (i.getDatum().getMonth() == d.getMonth())) {
                topDanaKorisnici k = new topDanaKorisnici();
                session.beginTransaction();
                query = session.createCriteria(Registracija.class);
                Registracija korisnik = (Registracija) query.add(Restrictions.eq("id", i.getIdKorisnika())).uniqueResult();
                session.getTransaction().commit();
                k.setId(korisnik.getId());
                k.setKorIme(korisnik.getKorisnickoIme());
                k.setRedniBr(redniBr++);
                k.setPoeni(i.getPoeni());
                k.setKorisnik(korisnik);
                listaDana.add(k);
            }
        }
        session.close();
        boolean top10 = true;
        topDanaKorisnici pomoc = new topDanaKorisnici();
        for (int i = 0; i < listaDana.size(); i++) {
            if (listaDana.get(i).getId() == reg.getId()) {
                if (i < 10) {
                    top10 = false;
                    pomoc.setId(listaDana.get(i).getId());
                    pomoc.setKorIme(listaDana.get(i).getKorIme());
                    pomoc.setPoeni(listaDana.get(i).getPoeni());
                    pomoc.setRedniBr(listaDana.get(i).getRedniBr());
                    pomoc.setKorisnik(reg);
                    break;
                }
            }
        }
        int kraj = listaDana.size();
        while (listaDana.size() > 10) {
            listaDana.remove(listaDana.size() - 1);
        }
        if (top10) {
            if (pomoc.getKorIme() == null) {
                pomoc.setId(reg.getId());
                pomoc.setKorIme(reg.getKorisnickoIme());
                pomoc.setPoeni(0);
                pomoc.setRedniBr(kraj + 1);
                pomoc.setKorisnik(reg);
            }
            listaDana.add(pomoc);
        }
    }

    public void vratiSe() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("gamePage.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(gameKontroler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<topDanaKorisnici> getListaDana() {
        return listaDana;
    }

    public void setListaDana(ArrayList<topDanaKorisnici> listaDana) {
        this.listaDana = listaDana;
    }

    public Registracija getReg() {
        return reg;
    }

    public BarChartModel getBarModel() {
        return barModel;
    }

    public void setBarModel(BarChartModel barModel) {
        this.barModel = barModel;
    }

    public void setReg(Registracija reg) {
        this.reg = reg;
    }

    public ArrayList<PartijeMoje> getMojePartije() {
        return mojePartije;
    }

    public void setMojePartije(ArrayList<PartijeMoje> mojePartije) {
        this.mojePartije = mojePartije;
    }

    public boolean isCekaj() {
        return cekaj;
    }

    public void setCekaj(boolean cekaj) {
        this.cekaj = cekaj;
    }

    public String getPoruka1() {
        return poruka1;
    }

    public void setPoruka1(String poruka1) {
        this.poruka1 = poruka1;
    }

    public String getPoruka2() {
        return poruka2;
    }

    public void setPoruka2(String poruka2) {
        this.poruka2 = poruka2;
    }

    public class topDanaKorisnici {

        private int redniBr;
        private int id;
        private String korIme;
        private Registracija korisnik;

        public Registracija getKorisnik() {
            return korisnik;
        }

        public void setKorisnik(Registracija korisnik) {
            this.korisnik = korisnik;
        }

        public String getKorIme() {
            return korIme;
        }

        public void setKorIme(String korIme) {
            this.korIme = korIme;
        }
        private int poeni;

        public int getRedniBr() {
            return redniBr;
        }

        public void setRedniBr(int redniBr) {
            this.redniBr = redniBr;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getPoeni() {
            return poeni;
        }

        public void setPoeni(int poeni) {
            this.poeni = poeni;
        }
    }

    public class PartijeMoje {

        private String imeProtivnika;
        private int poeniMoji;
        private int poeniProtivnika;
        private Date datum;
        private int idPartije;
        private org.primefaces.model.chart.BarChartModel barModel;

        public void init(int i1, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10) {
            barModel = new org.primefaces.model.chart.BarChartModel();

            ChartSeries moji = new ChartSeries();
            moji.setLabel("Moji poeni");
            moji.set("Slagalica", i1);
            moji.set("Moj broj", i2);
            moji.set("Skočko", i3);
            moji.set("Spojnice", i4);
            moji.set("Asocijacije", i5);

            ChartSeries protivnik = new ChartSeries();
            protivnik.setLabel("Protivnikovi poeni");
            protivnik.set("Slagalica", i6);
            protivnik.set("Moj broj", i7);
            protivnik.set("Skočko", i8);
            protivnik.set("Spojnice", i9);
            protivnik.set("Asocijacije", i10);

            barModel.addSeries(moji);
            barModel.addSeries(protivnik);
            barModel.setTitle("Statistika");
            barModel.setLegendPosition("ne");

            Axis xAxis = barModel.getAxis(AxisType.X);
            xAxis.setLabel("Igre");

            Axis yAxis = barModel.getAxis(AxisType.Y);
            yAxis.setLabel("Poeni");
            yAxis.setMin(0);
            yAxis.setMax(50);
        }

        public org.primefaces.model.chart.BarChartModel getBarModel() {
            return barModel;
        }

        public void setBarModel(org.primefaces.model.chart.BarChartModel barModel) {
            this.barModel = barModel;
        }

        public String getImeProtivnika() {
            return imeProtivnika;
        }

        public void setImeProtivnika(String imeProtivnika) {
            this.imeProtivnika = imeProtivnika;
        }

        public int getPoeniMoji() {
            return poeniMoji;
        }

        public void setPoeniMoji(int poeniMoji) {
            this.poeniMoji = poeniMoji;
        }

        public int getPoeniProtivnika() {
            return poeniProtivnika;
        }

        public void setPoeniProtivnika(int poeniProtivnika) {
            this.poeniProtivnika = poeniProtivnika;
        }

        public Date getDatum() {
            return datum;
        }

        public void setDatum(Date datum) {
            this.datum = datum;
        }

        public int getIdPartije() {
            return idPartije;
        }

        public void setIdPartije(int idPartije) {
            this.idPartije = idPartije;
        }

    }
}
