/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kontroleri;

import db.HibernateUtil;
import entiteti.Definisanjeigradana;
import entiteti.Igradana;
import entiteti.Igraslagalica;
import entiteti.Registracija;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@SessionScoped
public class IgradanaSlagalicaKontroler {

    private String slovo0, slovo1, slovo2, slovo3, slovo4, slovo5, slovo6,
            slovo7, slovo8, slovo9, slovo10, slovo11;
    private int vreme = 60;
    private String rec;
    private boolean uslov = false, dodatniUslov = false, gotovo = false;
    private int poen;
    private int lokacija = 0;
    private Random random;
    private String[] nizSlova = new String[]{"A", "B", "C", "Č", "Ć", "D", "Dž", "Đ", "E", "F", "G", "H", "I",
        "J", "K", "L", "Lj", "M", "N", "Nj", "O", "P", "R", "S", "Š", "T", "U", "V", "Z", "Ž", "A", "E", "I",
        "O", "U", "A", "E", "I", "O", "U"};

    public IgradanaSlagalicaKontroler() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession s = (HttpSession) fc.getExternalContext().getSession(false);
        Registracija reg = (Registracija) s.getAttribute("user");
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Igradana.class);
        query.addOrder(Order.desc("id"));
        Igradana igra = (Igradana) query.add(Restrictions.eq("idKorisnika", reg.getId())).setMaxResults(1).uniqueResult();
        session.getTransaction().commit();
        session.beginTransaction();
        query = session.createCriteria(Definisanjeigradana.class);
        ArrayList<Definisanjeigradana> def = (ArrayList<Definisanjeigradana>) query.list();
        session.getTransaction().commit();
        Definisanjeigradana temp = null;
        for (Definisanjeigradana d : def) {
            if ((new Date().getYear() == d.getDatum().getYear())
                    && (new Date().getDay() == d.getDatum().getDay())
                    && (new Date().getMonth() == d.getDatum().getMonth())) {
                temp = d;
                break;
            }
        }
        random = new Random(temp.getSeedSlagalica());
        session.close();
    }

    public void promenaVremena() {
        if (dodatniUslov) {
            if (vreme >= 0) {
                vreme--;
                if (vreme == 0) {
                    if (!gotovo) {
                        predajRec();
                    }
                }
            }
        }
    }

    public void predajRec() {
        dodatniUslov = false;
        gotovo = true;
        uslov = false;
        if (rec == null) {
            rec = " ";
        }

        ArrayList<String> slova = new ArrayList<>();
        slova.add(slovo0);
        slova.add(slovo1);
        slova.add(slovo2);
        slova.add(slovo3);
        slova.add(slovo4);
        slova.add(slovo5);
        slova.add(slovo6);
        slova.add(slovo7);
        slova.add(slovo8);
        slova.add(slovo9);
        slova.add(slovo10);
        slova.add(slovo11);

        int osvojeniPoeni = 0;
        rec = rec.toUpperCase();
        for (int i = 0; i < rec.length(); i++) {
            String temp = "" + rec.charAt(i);
            if (slova.contains(temp)) {
                osvojeniPoeni += 2;
                slova.remove(temp);
            } else {
                osvojeniPoeni = 0;
                break;
            }
        }
        if (!proveriPostojiRec(rec)) {
            osvojeniPoeni = 0;
        }

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession s = (HttpSession) fc.getExternalContext().getSession(false);
        Registracija reg = (Registracija) s.getAttribute("user");
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Igradana.class);
        query.addOrder(Order.desc("id"));
        Igradana igra = (Igradana) query.add(Restrictions.eq("idKorisnika", reg.getId())).setMaxResults(1).uniqueResult();
        session.getTransaction().commit();
        igra.setPoeni(igra.getPoeni() + osvojeniPoeni);
        igra.setPoeniiSlagalica(osvojeniPoeni);
        session.beginTransaction();
        session.update(igra);
        session.getTransaction().commit();
        session.close();
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("IgradanaSlagalica.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(slagalicaKontroler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void dalje() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("IgradanaMojBroj.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(slagalicaKontroler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void sledeceSlovo() {
        int br = random.nextInt(nizSlova.length);
        switch (lokacija) {
            case 0:
                slovo0 = nizSlova[br];
                break;
            case 1:
                slovo1 = nizSlova[br];
                break;
            case 2:
                slovo2 = nizSlova[br];
                break;
            case 3:
                slovo3 = nizSlova[br];
                break;
            case 4:
                slovo4 = nizSlova[br];
                break;
            case 5:
                slovo5 = nizSlova[br];
                break;
            case 6:
                slovo6 = nizSlova[br];
                break;
            case 7:
                slovo7 = nizSlova[br];
                break;
            case 8:
                slovo8 = nizSlova[br];
                break;
            case 9:
                slovo9 = nizSlova[br];
                break;
            case 10:
                slovo10 = nizSlova[br];
                break;
            case 11:
                slovo11 = nizSlova[br];
                break;
        }
        lokacija++;
        if (lokacija == 12) {
            uslov = true;
            dodatniUslov = true;
        }
    }

    public int getPoen() {
        return poen;
    }

    public boolean isGotovo() {
        return gotovo;
    }

    public void setGotovo(boolean gotovo) {
        this.gotovo = gotovo;
    }

    public void setPoen(int poen) {
        this.poen = poen;
    }

    public String getRec() {
        return rec;
    }

    public void setRec(String rec) {
        this.rec = rec;
    }

    public boolean isUslov() {
        return uslov;
    }

    public void setUslov(boolean uslov) {
        this.uslov = uslov;
    }

    public String getSlovo0() {
        return slovo0;
    }

    public void setSlovo0(String slovo0) {
        this.slovo0 = slovo0;
    }

    public String getSlovo1() {
        return slovo1;
    }

    public void setSlovo1(String slovo1) {
        this.slovo1 = slovo1;
    }

    public String getSlovo2() {
        return slovo2;
    }

    public void setSlovo2(String slovo2) {
        this.slovo2 = slovo2;
    }

    public String getSlovo3() {
        return slovo3;
    }

    public void setSlovo3(String slovo3) {
        this.slovo3 = slovo3;
    }

    public String getSlovo4() {
        return slovo4;
    }

    public void setSlovo4(String slovo4) {
        this.slovo4 = slovo4;
    }

    public String getSlovo5() {
        return slovo5;
    }

    public void setSlovo5(String slovo5) {
        this.slovo5 = slovo5;
    }

    public String getSlovo6() {
        return slovo6;
    }

    public void setSlovo6(String slovo6) {
        this.slovo6 = slovo6;
    }

    public String getSlovo7() {
        return slovo7;
    }

    public void setSlovo7(String slovo7) {
        this.slovo7 = slovo7;
    }

    public String getSlovo8() {
        return slovo8;
    }

    public void setSlovo8(String slovo8) {
        this.slovo8 = slovo8;
    }

    public String getSlovo9() {
        return slovo9;
    }

    public void setSlovo9(String slovo9) {
        this.slovo9 = slovo9;
    }

    public String getSlovo10() {
        return slovo10;
    }

    public void setSlovo10(String slovo10) {
        this.slovo10 = slovo10;
    }

    public String getSlovo11() {
        return slovo11;
    }

    public void setSlovo11(String slovo11) {
        this.slovo11 = slovo11;
    }

    public int getVreme() {
        return vreme;
    }

    public void setVreme(int vreme) {
        this.vreme = vreme;
    }

    private boolean proveriPostojiRec(String rec) {
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Igraslagalica.class);
        Igraslagalica igra = (Igraslagalica) query.add(Restrictions.eq("rec", rec.toLowerCase())).setMaxResults(1).uniqueResult();
        session.getTransaction().commit();
        session.close();
        if (igra == null) {
            return false;
        }
        return true;
    }

}
