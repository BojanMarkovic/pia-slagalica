/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kontroleri;

import db.HibernateUtil;
import entiteti.Definisanjeigradana;
import entiteti.Igradana;
import entiteti.Registracija;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@SessionScoped
public class IgradanaMojBrojKontroler {

    private boolean uslov = false;
    private int lokacija = 0;
    private String izraz, izraz2;
    private int broj0, broj1, broj2, broj3, dvocifren, trocifren, rezultat;
    private boolean dodatniUslov = false;
    private boolean gotovo = false;
    private int vreme = 60;
    private int poen;
    private Random random;

    public IgradanaMojBrojKontroler() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession s = (HttpSession) fc.getExternalContext().getSession(false);
        Registracija reg = (Registracija) s.getAttribute("user");
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Igradana.class);
        query.addOrder(Order.desc("id"));
        Igradana igra = (Igradana) query.add(Restrictions.eq("idKorisnika", reg.getId())).setMaxResults(1).uniqueResult();
        session.getTransaction().commit();
        session.beginTransaction();
        query = session.createCriteria(Definisanjeigradana.class);
        ArrayList<Definisanjeigradana> def = (ArrayList<Definisanjeigradana>) query.list();
        session.getTransaction().commit();
        Definisanjeigradana temp = null;
        for (Definisanjeigradana d : def) {
            if ((new Date().getYear() == d.getDatum().getYear())
                    && (new Date().getDay() == d.getDatum().getDay())
                    && (new Date().getMonth() == d.getDatum().getMonth())) {
                temp = d;
                break;
            }
        }
        session.close();
        random = new Random(temp.getSeedMojBroj());
        poen = igra.getPoeni();
    }

    public void promenaVremena() {
        if (dodatniUslov) {
            if (vreme >= 0) {
                vreme--;
                if (vreme == 0) {
                    if (!gotovo) {
                        predajIzraz();
                    }
                }
            }
        }
    }

    public void sledeciBroj() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession s = (HttpSession) fc.getExternalContext().getSession(false);
        Registracija reg = (Registracija) s.getAttribute("user");
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Igradana.class);
        query.addOrder(Order.desc("id"));
        Igradana igra = (Igradana) query.add(Restrictions.eq("idKorisnika", reg.getId())).setMaxResults(1).uniqueResult();
        session.getTransaction().commit();
        session.close();
        switch (lokacija) {
            case 0:
                broj0 = 1 + random.nextInt(9);
                break;
            case 1:
                broj1 = 1 + random.nextInt(9);
                break;
            case 2:
                broj2 = 1 + random.nextInt(9);
                break;
            case 3:
                broj3 = 1 + random.nextInt(9);
                break;
            case 4:
                dvocifren = 5 * (2 + random.nextInt(3));
                break;
            case 5:
                trocifren = 25 * (1 + random.nextInt(3));
                break;
            case 6:
                rezultat = 1 + random.nextInt(999);
                break;

        }
        lokacija++;
        if (lokacija == 7) {
            uslov = true;
            dodatniUslov = true;
        }
    }

    public void predajIzraz() {
        dodatniUslov = false;
        gotovo = true;
        uslov = false;
        if (izraz == null) {
            izraz = " ";
        }

        ArrayList<String> brojevi = new ArrayList<>();
        brojevi.add("" + broj0);
        brojevi.add("" + broj1);
        brojevi.add("" + broj2);
        brojevi.add("" + broj3);
        brojevi.add("" + dvocifren);
        brojevi.add("" + trocifren);
        ArrayList<String> znakovi = new ArrayList<>();
        znakovi.add("+");
        znakovi.add("-");
        znakovi.add("*");
        znakovi.add("/");
        znakovi.add("(");
        znakovi.add(")");
        ArrayList<String> cifre = new ArrayList<>();
        cifre.add("0");
        cifre.add("1");
        cifre.add("2");
        cifre.add("3");
        cifre.add("4");
        cifre.add("5");
        cifre.add("6");
        cifre.add("7");
        cifre.add("8");
        cifre.add("9");

        boolean validnost = true;
        for (int i = 0; i < izraz.length(); i++) {
            String temp = "" + izraz.charAt(i);
            if (!znakovi.contains(temp)) {
                if (i + 1 < izraz.length()) {
                    if (cifre.contains("" + izraz.charAt(i + 1))) {
                        i++;
                        temp += izraz.charAt(i);
                    }
                }
                if (i + 1 < izraz.length()) {
                    if (cifre.contains("" + izraz.charAt(i + 1))) {
                        i++;
                        temp += izraz.charAt(i);
                    }
                }
            }
            if (brojevi.contains(temp)) {
                brojevi.remove(temp);
            } else {
                if (!znakovi.contains(temp)) {
                    validnost = false;
                    break;
                }
            }
        }
        int osvojeniPoeni = -1;
        if (validnost) {
            try {
                osvojeniPoeni = (int) new ScriptEngineManager().getEngineByName("JavaScript").eval(izraz);
            } catch (ScriptException ex) {
                Logger.getLogger(MojBrojKontroler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (osvojeniPoeni == Integer.parseInt(izraz2)) {
            if (osvojeniPoeni < 0) {
                osvojeniPoeni = 0;
            } else {
                if (osvojeniPoeni == rezultat) {
                    osvojeniPoeni = 10;
                } else if (Math.abs(osvojeniPoeni - rezultat) < 100) {
                    osvojeniPoeni = 5;
                } else {
                    osvojeniPoeni = 0;
                }
            }
        } else {
            osvojeniPoeni = 0;
        }

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession s = (HttpSession) fc.getExternalContext().getSession(false);
        Registracija reg = (Registracija) s.getAttribute("user");
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Igradana.class);
        query.addOrder(Order.desc("id"));
        Igradana igra = (Igradana) query.add(Restrictions.eq("idKorisnika", reg.getId())).setMaxResults(1).uniqueResult();
        session.getTransaction().commit();
        igra.setPoeni(igra.getPoeni() + osvojeniPoeni);
        igra.setPoeniMojBroj(osvojeniPoeni);
        session.beginTransaction();
        session.update(igra);
        session.getTransaction().commit();
        session.close();
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("IgradanaMojBroj.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(slagalicaKontroler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void dalje() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("IgradanaSkocko.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(slagalicaKontroler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getPoen() {
        return poen;
    }

    public void setPoen(int poen) {
        this.poen = poen;
    }

    public boolean isUslov() {
        return uslov;
    }

    public void setUslov(boolean uslov) {
        this.uslov = uslov;
    }

    public String getIzraz() {
        return izraz;
    }

    public void setIzraz(String izraz) {
        this.izraz = izraz;
    }

    public int getBroj0() {
        return broj0;
    }

    public String getIzraz2() {
        return izraz2;
    }

    public void setIzraz2(String izraz2) {
        this.izraz2 = izraz2;
    }

    public void setBroj0(int broj0) {
        this.broj0 = broj0;
    }

    public int getBroj1() {
        return broj1;
    }

    public void setBroj1(int broj1) {
        this.broj1 = broj1;
    }

    public int getBroj2() {
        return broj2;
    }

    public boolean isGotovo() {
        return gotovo;
    }

    public void setGotovo(boolean gotovo) {
        this.gotovo = gotovo;
    }

    public void setBroj2(int broj2) {
        this.broj2 = broj2;
    }

    public int getBroj3() {
        return broj3;
    }

    public void setBroj3(int broj3) {
        this.broj3 = broj3;
    }

    public int getDvocifren() {
        return dvocifren;
    }

    public void setDvocifren(int dvocifren) {
        this.dvocifren = dvocifren;
    }

    public int getTrocifren() {
        return trocifren;
    }

    public void setTrocifren(int trocifren) {
        this.trocifren = trocifren;
    }

    public int getRezultat() {
        return rezultat;
    }

    public void setRezultat(int rezultat) {
        this.rezultat = rezultat;
    }

    public int getVreme() {
        return vreme;
    }

    public void setVreme(int vreme) {
        this.vreme = vreme;
    }

}
