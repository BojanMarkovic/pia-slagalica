/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kontroleri;

import db.HibernateUtil;
import entiteti.Definisanjeigradana;
import entiteti.Igradana;
import entiteti.Igraspojnice;
import entiteti.Registracija;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@SessionScoped
public class IgradanaSpojnicaKontroler {

    private boolean uslov = false;
    private int lokacija = 0, osvojeniPoeni = 0;
    private boolean dodatniUslov = false;
    private boolean gotovo = false;
    private int vreme = 90;
    private int poen;
    private ArrayList<Igraspojnice> listaSpojeva;
    private ArrayList<spojnice> listaZaIspis;

    public IgradanaSpojnicaKontroler() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession s = (HttpSession) fc.getExternalContext().getSession(false);
        Registracija reg = (Registracija) s.getAttribute("user");
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();

        session.beginTransaction();
        Criteria query = session.createCriteria(Igradana.class);
        query.addOrder(Order.desc("id"));
        Igradana igra = (Igradana) query.add(Restrictions.eq("idKorisnika", reg.getId())).setMaxResults(1).uniqueResult();
        session.getTransaction().commit();

        session.beginTransaction();
        query = session.createCriteria(Igraspojnice.class);
        query.addOrder(Order.desc("id"));
        Igraspojnice t = (Igraspojnice) query.setMaxResults(1).uniqueResult();
        session.getTransaction().commit();

        session.beginTransaction();
        query = session.createCriteria(Definisanjeigradana.class);
        ArrayList<Definisanjeigradana> def = (ArrayList<Definisanjeigradana>) query.list();
        session.getTransaction().commit();
        Definisanjeigradana temp2 = null;
        for (Definisanjeigradana d : def) {
            if ((new Date().getYear() == d.getDatum().getYear())
                    && (new Date().getDay() == d.getDatum().getDay())
                    && (new Date().getMonth() == d.getDatum().getMonth())) {
                temp2 = d;
                break;
            }
        }

        Random random = new Random(System.currentTimeMillis());
        session.beginTransaction();
        query = session.createCriteria(Igraspojnice.class);
        listaSpojeva = (ArrayList<Igraspojnice>) query.add(Restrictions.eq("vrsta", temp2.getVrstaSpojnica())).list();
        session.getTransaction().commit();
        session.close();
        poen = igra.getPoeni();
        listaZaIspis = new ArrayList<>();
        ArrayList<Integer> niz = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            niz.add(i);
        }
        for (Igraspojnice i : listaSpojeva) {
            spojnice temp = new spojnice();
            temp.setIgra(new Igraspojnice());
            temp.getIgra().setPojam1(i.getPojam1());
            int x = random.nextInt(niz.size());
            temp.getIgra().setPojam2(listaSpojeva.get(niz.get(x)).getPojam2());
            niz.remove(x);
            temp.tacno = false;
            listaZaIspis.add(temp);
        }
        dodatniUslov = true;
        listaZaIspis.get(0).trenutnaLokacija = true;
    }

    public void promenaVremena() {
        if (dodatniUslov) {
            if (vreme >= 0) {
                vreme--;
                if (vreme == 0) {
                    if (!gotovo) {
                        zavrsi();
                    }
                }
            }
        }
    }

    public void test(String s) {
        if (listaSpojeva.get(lokacija).getPojam2().compareTo(s) == 0) {
            osvojeniPoeni++;
            poen++;
            int i = 0;
            for (spojnice sp : listaZaIspis) {
                if (sp.igra.getPojam2().compareTo(s) == 0) {
                    break;
                }
                i++;
            }
            listaZaIspis.get(i).tacno = true;
            listaZaIspis.get(lokacija).prviTacno = 1;
        } else {
            listaZaIspis.get(lokacija).prviTacno = 2;
        }
        listaZaIspis.get(lokacija).trenutnaLokacija = true;
        lokacija++;
        if (lokacija == 10) {
            zavrsi();
        }
    }

    public void dalje() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("IgradanaAsocijacije.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(slagalicaKontroler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void zavrsi() {
        dodatniUslov = false;
        gotovo = true;
        uslov = false;
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession s = (HttpSession) fc.getExternalContext().getSession(false);
        Registracija reg = (Registracija) s.getAttribute("user");
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Igradana.class);
        query.addOrder(Order.desc("id"));
        Igradana igra = (Igradana) query.add(Restrictions.eq("idKorisnika", reg.getId())).setMaxResults(1).uniqueResult();
        session.getTransaction().commit();
        igra.setPoeni(igra.getPoeni() + osvojeniPoeni);
        igra.setPoeniSpojnice(osvojeniPoeni);
        session.beginTransaction();
        session.update(igra);
        session.getTransaction().commit();
        session.close();
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("IgradanaSpojnice.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(slagalicaKontroler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<spojnice> getListaZaIspis() {
        return listaZaIspis;
    }

    public void setListaZaIspis(ArrayList<spojnice> listaZaIspis) {
        this.listaZaIspis = listaZaIspis;
    }

    public boolean isUslov() {
        return uslov;
    }

    public void setUslov(boolean uslov) {
        this.uslov = uslov;
    }

    public int getVreme() {
        return vreme;
    }

    public void setVreme(int vreme) {
        this.vreme = vreme;
    }

    public int getPoen() {
        return poen;
    }

    public void setPoen(int poen) {
        this.poen = poen;
    }

    public class spojnice {

        private Igraspojnice igra;
        private boolean tacno = false;
        private int prviTacno = 0;
        private boolean trenutnaLokacija = false;

        public boolean isTrenutnaLokacija() {
            return trenutnaLokacija;
        }

        public void setTrenutnaLokacija(boolean trenutnaLokacija) {
            this.trenutnaLokacija = trenutnaLokacija;
        }

        public int getPrviTacno() {
            return prviTacno;
        }

        public void setPrviTacno(int prviTacno) {
            this.prviTacno = prviTacno;
        }

        public Igraspojnice getIgra() {
            return igra;
        }

        public void setIgra(Igraspojnice igra) {
            this.igra = igra;
        }

        public boolean isTacno() {
            return tacno;
        }

        public void setTacno(boolean tacno) {
            this.tacno = tacno;
        }

    }

    public boolean isGotovo() {
        return gotovo;
    }

    public void setGotovo(boolean gotovo) {
        this.gotovo = gotovo;
    }

    public int getLokacija() {
        return lokacija;
    }

    public void setLokacija(int lokacija) {
        this.lokacija = lokacija;
    }

}
