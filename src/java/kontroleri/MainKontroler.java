/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kontroleri;

import entiteti.Registracija;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

@ManagedBean
@SessionScoped
public class MainKontroler {

    private String korisnik;

    public String getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(String korisnik) {
        this.korisnik = korisnik;
    }

    public void checkAlreadyLoggedinSuperVizor() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        Registracija user = (Registracija) hs.getAttribute("user");
        if (user != null) {
            korisnik = "Trenutno ulogovan korisnik: " + user.getIme();
        }
        if ((user == null) || (user.getTip() != new Byte("2"))) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("PocetnaStrana.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(MainKontroler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void checkAlreadyLoggedinAdmin() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        Registracija user = (Registracija) hs.getAttribute("user");
        if (user != null) {
            korisnik = "Trenutno ulogovan korisnik: " + user.getIme();
        }
        if ((user == null) || (user.getTip() != new Byte("1"))) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("PocetnaStrana.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(MainKontroler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void checkAlreadyLoggedinGame() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession hs = (HttpSession) fc.getExternalContext().getSession(false);
        Registracija user = (Registracija) hs.getAttribute("user");
        if (user != null) {
            korisnik = "Trenutno ulogovan korisnik: " + user.getIme();
        }
        if ((user == null) || (user.getTip() != new Byte("3"))) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("PocetnaStrana.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(MainKontroler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
