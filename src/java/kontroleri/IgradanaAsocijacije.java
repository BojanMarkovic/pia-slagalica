/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kontroleri;

import db.HibernateUtil;
import entiteti.Definisanjeigradana;
import entiteti.Igraasocijacije;
import entiteti.Igradana;
import entiteti.Registracija;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@SessionScoped
public class IgradanaAsocijacije {

    private boolean uslov = false;
    private int osvojeniPoeni = 0;
    private boolean dodatniUslov = false;
    private boolean gotovo = false;
    private int vreme = 240;
    private int poen;
    private Kolona A, B, C, D;
    private String konacnoResenje;
    private Igraasocijacije igra;

    public IgradanaAsocijacije() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession s = (HttpSession) fc.getExternalContext().getSession(false);
        Registracija reg = (Registracija) s.getAttribute("user");
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();

        session.beginTransaction();
        Criteria query = session.createCriteria(Igradana.class);
        query.addOrder(Order.desc("id"));
        Igradana igraDana = (Igradana) query.add(Restrictions.eq("idKorisnika", reg.getId())).setMaxResults(1).uniqueResult();
        session.getTransaction().commit();

        session.beginTransaction();
        query = session.createCriteria(Igraasocijacije.class);
        query.addOrder(Order.desc("id"));
        Igraasocijacije t = (Igraasocijacije) query.setMaxResults(1).uniqueResult();
        session.getTransaction().commit();

        session.beginTransaction();
        query = session.createCriteria(Definisanjeigradana.class);
        ArrayList<Definisanjeigradana> def = (ArrayList<Definisanjeigradana>) query.list();
        session.getTransaction().commit();
        Definisanjeigradana temp = null;
        for (Definisanjeigradana d : def) {
            if ((new Date().getYear() == d.getDatum().getYear())
                    && (new Date().getDay() == d.getDatum().getDay())
                    && (new Date().getMonth() == d.getDatum().getMonth())) {
                temp = d;
                break;
            }
        }

        session.beginTransaction();
        query = session.createCriteria(Igraasocijacije.class);
        igra = (Igraasocijacije) query.add(Restrictions.eq("id", temp.getIdAsocijacije())).uniqueResult();
        session.getTransaction().commit();
        session.close();
        poen = igraDana.getPoeni();

        A = new Kolona("A");
        B = new Kolona("B");
        C = new Kolona("C");
        D = new Kolona("D");
        dodatniUslov = true;
    }

    public void promenaVremena() {
        if (dodatniUslov) {
            if (vreme >= 0) {
                vreme--;
                if (vreme == 0) {
                    if (!gotovo) {
                        otvoriSvaPolja();
                        A.gotova = false;
                        B.gotova = false;
                        C.gotova = false;
                        D.gotova = false;
                        zavrsi();
                    }
                }
            }
        }
    }

    public void otvoriPolje(String s) {
        if (s.contains("A")) {
            String[] kolona = igra.getKolonaA().split(", ");
            if (s.contains("1")) {
                A.red1 = kolona[0];
            } else if (s.contains("2")) {
                A.red2 = kolona[1];
            } else if (s.contains("3")) {
                A.red3 = kolona[2];
            } else if (s.contains("4")) {
                A.red4 = kolona[3];
            }
        } else if (s.contains("B")) {
            String[] kolona = igra.getKolonaB().split(", ");
            if (s.contains("1")) {
                B.red1 = kolona[0];
            } else if (s.contains("2")) {
                B.red2 = kolona[1];
            } else if (s.contains("3")) {
                B.red3 = kolona[2];
            } else if (s.contains("4")) {
                B.red4 = kolona[3];
            }
        } else if (s.contains("C")) {
            String[] kolona = igra.getKolonaC().split(", ");
            if (s.contains("1")) {
                C.red1 = kolona[0];
            } else if (s.contains("2")) {
                C.red2 = kolona[1];
            } else if (s.contains("3")) {
                C.red3 = kolona[2];
            } else if (s.contains("4")) {
                C.red4 = kolona[3];
            }
        } else if (s.contains("D")) {
            String[] kolona = igra.getKolonaD().split(", ");
            if (s.contains("1")) {
                D.red1 = kolona[0];
            } else if (s.contains("2")) {
                D.red2 = kolona[1];
            } else if (s.contains("3")) {
                D.red3 = kolona[2];
            } else if (s.contains("4")) {
                D.red4 = kolona[3];
            }
        }
    }

    public void predajOdgovor() {
        if (!konacnoResenje.isEmpty()) {
            boolean pobeda = false;
            if (igra.getKonacnoResenje().toLowerCase().compareTo(konacnoResenje.toLowerCase()) == 0) {
                pobeda = true;
            } else {
                if (igra.getSinonimi() != null) {
                    if (igra.getSinonimi().toLowerCase().contains(konacnoResenje.toLowerCase())) {
                        pobeda = true;
                    }
                }
            }
            if (pobeda) {
                osvojeniPoeni += 10;
                poen += 10;
                if (A.getKonacnoRes().isEmpty()) {
                    osvojeniPoeni += 5;
                    poen += 5;
                }
                if (B.getKonacnoRes().isEmpty()) {
                    osvojeniPoeni += 5;
                    poen += 5;
                }
                if (C.getKonacnoRes().isEmpty()) {
                    osvojeniPoeni += 5;
                    poen += 5;
                }
                if (D.getKonacnoRes().isEmpty()) {
                    osvojeniPoeni += 5;
                    poen += 5;
                }
                otvoriSvaPolja();
                zavrsi();
            }
        } else {
            if (!A.gotova) {
                if (!A.konacnoRes.isEmpty()) {
                    String[] kolonaA = igra.getKolonaA().split(", ");
                    if (A.konacnoRes.toLowerCase().compareTo(kolonaA[4].toLowerCase()) == 0) {
                        otvoriSvaPoljaA();
                        osvojeniPoeni += 5;
                        poen += 5;
                    }
                }
            }
            if (!B.gotova) {
                if (!B.konacnoRes.isEmpty()) {
                    String[] kolonaB = igra.getKolonaB().split(", ");
                    if (B.konacnoRes.toLowerCase().compareTo(kolonaB[4].toLowerCase()) == 0) {
                        otvoriSvaPoljaB();
                        osvojeniPoeni += 5;
                        poen += 5;
                    }
                }
            }
            if (!C.gotova) {
                if (!C.konacnoRes.isEmpty()) {
                    String[] kolonaC = igra.getKolonaC().split(", ");
                    if (C.konacnoRes.toLowerCase().compareTo(kolonaC[4].toLowerCase()) == 0) {
                        otvoriSvaPoljaC();
                        osvojeniPoeni += 5;
                        poen += 5;
                    }
                }
            }
            if (!D.gotova) {
                if (!D.konacnoRes.isEmpty()) {
                    String[] kolonaD = igra.getKolonaD().split(", ");
                    if (D.konacnoRes.toLowerCase().compareTo(kolonaD[4].toLowerCase()) == 0) {
                        otvoriSvaPoljaD();
                        osvojeniPoeni += 5;
                        poen += 5;
                    }
                }
            }
        }
    }

    private void otvoriSvaPoljaA() {
        String[] kolonaA = igra.getKolonaA().split(", ");
        A.red1 = kolonaA[0];
        A.red2 = kolonaA[1];
        A.red3 = kolonaA[2];
        A.red4 = kolonaA[3];
        A.konacnoRes = kolonaA[4];
        A.gotova = true;
    }

    private void otvoriSvaPoljaB() {
        String[] kolonaB = igra.getKolonaB().split(", ");
        B.red1 = kolonaB[0];
        B.red2 = kolonaB[1];
        B.red3 = kolonaB[2];
        B.red4 = kolonaB[3];
        B.konacnoRes = kolonaB[4];
        B.gotova = true;
    }

    private void otvoriSvaPoljaC() {
        String[] kolonaC = igra.getKolonaC().split(", ");
        C.red1 = kolonaC[0];
        C.red2 = kolonaC[1];
        C.red3 = kolonaC[2];
        C.red4 = kolonaC[3];
        C.konacnoRes = kolonaC[4];
        C.gotova = true;
    }

    private void otvoriSvaPoljaD() {
        String[] kolonaD = igra.getKolonaD().split(", ");
        D.red1 = kolonaD[0];
        D.red2 = kolonaD[1];
        D.red3 = kolonaD[2];
        D.red4 = kolonaD[3];
        D.konacnoRes = kolonaD[4];
        D.gotova = true;
    }

    private void otvoriSvaPolja() {
        otvoriSvaPoljaA();
        otvoriSvaPoljaB();
        otvoriSvaPoljaC();
        otvoriSvaPoljaD();
        konacnoResenje = igra.getKonacnoResenje() + " ";
        if (igra.getSinonimi() != null) {
            konacnoResenje += igra.getSinonimi();
        }
    }

    private void zavrsi() {
        dodatniUslov = false;
        gotovo = true;
        uslov = false;
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession s = (HttpSession) fc.getExternalContext().getSession(false);
        Registracija reg = (Registracija) s.getAttribute("user");
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Igradana.class);
        query.addOrder(Order.desc("id"));
        Igradana igra = (Igradana) query.add(Restrictions.eq("idKorisnika", reg.getId())).setMaxResults(1).uniqueResult();
        session.getTransaction().commit();
        igra.setPoeni(igra.getPoeni() + osvojeniPoeni);
        igra.setPoeniAsocijacije(osvojeniPoeni);
        session.beginTransaction();
        session.update(igra);
        session.getTransaction().commit();
        session.close();
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("IgradanaAsocijacije.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(slagalicaKontroler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void dalje() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("IgradanaPoeni.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(slagalicaKontroler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Kolona getA() {
        return A;
    }

    public void setA(Kolona A) {
        this.A = A;
    }

    public Kolona getB() {
        return B;
    }

    public void setB(Kolona B) {
        this.B = B;
    }

    public Kolona getC() {
        return C;
    }

    public void setC(Kolona C) {
        this.C = C;
    }

    public Kolona getD() {
        return D;
    }

    public void setD(Kolona D) {
        this.D = D;
    }

    public String getKonacnoResenje() {
        return konacnoResenje;
    }

    public void setKonacnoResenje(String konacnoResenje) {
        this.konacnoResenje = konacnoResenje;
    }

    public boolean isUslov() {
        return uslov;
    }

    public void setUslov(boolean uslov) {
        this.uslov = uslov;
    }

    public int getOsvojeniPoeni() {
        return osvojeniPoeni;
    }

    public void setOsvojeniPoeni(int osvojeniPoeni) {
        this.osvojeniPoeni = osvojeniPoeni;
    }

    public boolean isGotovo() {
        return gotovo;
    }

    public void setGotovo(boolean gotovo) {
        this.gotovo = gotovo;
    }

    public int getVreme() {
        return vreme;
    }

    public void setVreme(int vreme) {
        this.vreme = vreme;
    }

    public int getPoen() {
        return poen;
    }

    public void setPoen(int poen) {
        this.poen = poen;
    }

    public class Kolona {

        private String red1, red2, red3, red4, konacnoRes;
        private boolean gotova = false;

        public Kolona(String s) {
            red1 = s + "1";
            red2 = s + "2";
            red3 = s + "3";
            red4 = s + "4";
        }

        public String getRed1() {
            return red1;
        }

        public void setRed1(String red1) {
            this.red1 = red1;
        }

        public String getRed2() {
            return red2;
        }

        public void setRed2(String red2) {
            this.red2 = red2;
        }

        public String getRed3() {
            return red3;
        }

        public void setRed3(String red3) {
            this.red3 = red3;
        }

        public String getRed4() {
            return red4;
        }

        public void setRed4(String red4) {
            this.red4 = red4;
        }

        public String getKonacnoRes() {
            return konacnoRes;
        }

        public void setKonacnoRes(String konacnoRes) {
            this.konacnoRes = konacnoRes;
        }

        public boolean isGotova() {
            return gotova;
        }

        public void setGotova(boolean gotova) {
            this.gotova = gotova;
        }

    }

}
