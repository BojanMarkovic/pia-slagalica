/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kontroleri;

import db.HibernateUtil;
import entiteti.Definisanjeigradana;
import entiteti.Igradana;
import entiteti.Registracija;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@SessionScoped
public class IgradanaSkockoKontroler {

    private boolean uslov = false;
    private int lokacija = 0, lokacijaRezultata = 0;
    private String kombinacija;
    private boolean dodatniUslov = false;
    private boolean gotovo = false;
    private boolean master = false;
    private int vreme = 90;
    private int poen;
    private ArrayList<Kombinacija> listaKombinacija, listaRezultata;

    public IgradanaSkockoKontroler() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession s = (HttpSession) fc.getExternalContext().getSession(false);
        Registracija reg = (Registracija) s.getAttribute("user");
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Igradana.class);
        query.addOrder(Order.desc("id"));
        Igradana igra = (Igradana) query.add(Restrictions.eq("idKorisnika", reg.getId())).setMaxResults(1).uniqueResult();
        session.getTransaction().commit();
        session.beginTransaction();
        query = session.createCriteria(Definisanjeigradana.class);
        ArrayList<Definisanjeigradana> def = (ArrayList<Definisanjeigradana>) query.list();
        session.getTransaction().commit();
        Definisanjeigradana temp2 = null;
        for (Definisanjeigradana d : def) {
            if ((new Date().getYear() == d.getDatum().getYear())
                    && (new Date().getDay() == d.getDatum().getDay())
                    && (new Date().getMonth() == d.getDatum().getMonth())) {
                temp2 = d;
                break;
            }
        }
        session.close();
        poen = igra.getPoeni();
        listaKombinacija = new ArrayList<>();
        listaRezultata = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            listaKombinacija.add(new Kombinacija());
            if (i != 6) {
                listaRezultata.add(new Kombinacija());
            }
        }
        Random temp = new Random(temp2.getSeedSkocko());
        kombinacija = temp.nextInt(6) + " ";
        kombinacija += temp.nextInt(6);
        kombinacija += " " + temp.nextInt(6);
        kombinacija += " " + temp.nextInt(6);
    }

    public void obrisiSimbol() {
        if (lokacija == 0) {
            return;
        }
        lokacija--;
        int temp = lokacija % 4;
        switch (temp) {
            case 0:
                listaKombinacija.get(lokacija / 4).setA1(null);
                listaKombinacija.get(lokacija / 4).setSlika1(postaviSliku("8"));
                break;
            case 1:
                listaKombinacija.get(lokacija / 4).setA2(null);
                listaKombinacija.get(lokacija / 4).setSlika2(postaviSliku("8"));
                break;
            case 2:
                listaKombinacija.get(lokacija / 4).setA3(null);
                listaKombinacija.get(lokacija / 4).setSlika3(postaviSliku("8"));
                break;
            case 3:
                listaKombinacija.get(lokacija / 4).setA4(null);
                listaKombinacija.get(lokacija / 4).setSlika4(postaviSliku("8"));
                break;
        }
    }

    public void sledeciSimbol(String x) {
        if (!gotovo) {
            if (lokacija == 24) {
                uslov = false;
                predajKombinaciju();
            }
        }
        if (lokacija == 0) {
            dodatniUslov = true;
        }
        int temp = lokacija % 4;
        switch (temp) {
            case 0:
                listaKombinacija.get(lokacija / 4).setA1(x);
                listaKombinacija.get(lokacija / 4).setSlika1(postaviSliku(x));
                break;
            case 1:
                listaKombinacija.get(lokacija / 4).setA2(x);
                listaKombinacija.get(lokacija / 4).setSlika2(postaviSliku(x));
                break;
            case 2:
                listaKombinacija.get(lokacija / 4).setA3(x);
                listaKombinacija.get(lokacija / 4).setSlika3(postaviSliku(x));
                break;
            case 3:
                listaKombinacija.get(lokacija / 4).setA4(x);
                listaKombinacija.get(lokacija / 4).setSlika4(postaviSliku(x));
                break;
        }
        lokacija++;
        if (lokacija % 4 == 0) {
            uslov = true;
        }
    }

    private String postaviSliku(String x) {
        switch (Integer.parseInt(x)) {
            case 0:
                return "/resources/images/skocko.png";
            case 1:
                return "/resources/images/zvezda.png";
            case 2:
                return "/resources/images/tref.png";
            case 3:
                return "/resources/images/karo.png";
            case 4:
                return "/resources/images/srce.png";
            case 5:
                return "/resources/images/pik.png";
        }
        return "";
    }

    private String postaviSlikuResenja(String x) {
        switch (Integer.parseInt(x)) {
            case 0:
                return "/resources/images/tacno.png";
            case 1:
                return "/resources/images/mozda.png";
            case 2:
                return "/resources/images/netacno.png";
        }
        return "";
    }

    public void promenaVremena() {
        if (dodatniUslov) {
            if (vreme >= 0) {
                vreme--;
                if (vreme == 0) {
                    if (!gotovo) {
                        master = true;
                        predajKombinaciju();
                    }
                }
            }
        }
    }

    public void predajKombinaciju() {
        uslov = false;
        int temp = lokacija / 4 - 1;
        String rez = listaKombinacija.get(temp).a1 + " ";
        rez += listaKombinacija.get(temp).a2;
        rez += " " + listaKombinacija.get(temp).a3;
        rez += " " + listaKombinacija.get(temp).a4;
        if (rez.compareTo(kombinacija) == 0) {
            master = true;
            dodatniUslov = false;
            gotovo = true;
            listaRezultata.get(lokacijaRezultata % 7).setA1("+");
            listaRezultata.get(lokacijaRezultata % 7).setA2("+");
            listaRezultata.get(lokacijaRezultata % 7).setA3("+");
            listaRezultata.get(lokacijaRezultata % 7).setA4("+");
            listaRezultata.get(lokacijaRezultata % 7).setSlika1(postaviSlikuResenja("0"));
            listaRezultata.get(lokacijaRezultata % 7).setSlika2(postaviSlikuResenja("0"));
            listaRezultata.get(lokacijaRezultata % 7).setSlika3(postaviSlikuResenja("0"));
            listaRezultata.get(lokacijaRezultata % 7).setSlika4(postaviSlikuResenja("0"));
            lokacijaRezultata++;
            int osvojeniPoeni = 10;
            FacesContext fc = FacesContext.getCurrentInstance();
            HttpSession s = (HttpSession) fc.getExternalContext().getSession(false);
            Registracija reg = (Registracija) s.getAttribute("user");
            SessionFactory sessionF = HibernateUtil.getSessionFactory();
            Session session = sessionF.openSession();
            session.beginTransaction();
            Criteria query = session.createCriteria(Igradana.class);
            query.addOrder(Order.desc("id"));
            Igradana igra = (Igradana) query.add(Restrictions.eq("idKorisnika", reg.getId())).setMaxResults(1).uniqueResult();
            session.getTransaction().commit();
            igra.setPoeni(igra.getPoeni() + osvojeniPoeni);
            igra.setPoeniSkocko(osvojeniPoeni);
            session.beginTransaction();
            session.update(igra);
            session.getTransaction().commit();
            session.close();
        } else {
            String[] niz = kombinacija.split(" ");
            ArrayList<String> t = new ArrayList<>();
            for (String s : niz) {
                t.add(s);
            }
            int sadrzi = 0;
            int tacno = 0;
            Kombinacija komb = new Kombinacija();
            komb.a1 = listaKombinacija.get(temp).a1;
            komb.a2 = listaKombinacija.get(temp).a2;
            komb.a3 = listaKombinacija.get(temp).a3;
            komb.a4 = listaKombinacija.get(temp).a4;
            if (listaKombinacija.get(temp).a1.compareTo(niz[0]) == 0) {
                tacno++;
                t.remove(listaKombinacija.get(temp).a1);
                listaKombinacija.get(temp).a1 = "-1";
            }
            if (listaKombinacija.get(temp).a2.compareTo(niz[1]) == 0) {
                tacno++;
                t.remove(listaKombinacija.get(temp).a2);
                listaKombinacija.get(temp).a2 = "-1";
            }
            if (listaKombinacija.get(temp).a3.compareTo(niz[2]) == 0) {
                tacno++;
                t.remove(listaKombinacija.get(temp).a3);
                listaKombinacija.get(temp).a3 = "-1";
            }
            if (listaKombinacija.get(temp).a4.compareTo(niz[3]) == 0) {
                tacno++;
                t.remove(listaKombinacija.get(temp).a4);
                listaKombinacija.get(temp).a4 = "-1";
            }
            if (t.contains(listaKombinacija.get(temp).a1)) {
                sadrzi++;
                t.remove(listaKombinacija.get(temp).a1);
            }
            if (t.contains(listaKombinacija.get(temp).a2)) {
                sadrzi++;
                t.remove(listaKombinacija.get(temp).a2);
            }
            if (t.contains(listaKombinacija.get(temp).a3)) {
                sadrzi++;
                t.remove(listaKombinacija.get(temp).a3);
            }
            if (t.contains(listaKombinacija.get(temp).a4)) {
                sadrzi++;
                t.remove(listaKombinacija.get(temp).a4);
            }
            for (int i = 0; i < tacno; i++) {
                if (i == 0) {
                    listaRezultata.get(lokacijaRezultata % 7).setA1("+");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika1(postaviSlikuResenja("0"));
                }
                if (i == 1) {
                    listaRezultata.get(lokacijaRezultata % 7).setA2("+");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika2(postaviSlikuResenja("0"));
                }
                if (i == 2) {
                    listaRezultata.get(lokacijaRezultata % 7).setA3("+");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika3(postaviSlikuResenja("0"));
                }
                if (i == 3) {
                    listaRezultata.get(lokacijaRezultata % 7).setA4("+");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika4(postaviSlikuResenja("0"));
                }
            }
            listaKombinacija.get(temp).a1 = komb.a1;
            listaKombinacija.get(temp).a2 = komb.a2;
            listaKombinacija.get(temp).a3 = komb.a3;
            listaKombinacija.get(temp).a4 = komb.a4;
            int sadrzi2 = sadrzi;
            for (int i = tacno; (i < 4) && (sadrzi != 0); i++) {
                if (i == 0) {
                    listaRezultata.get(lokacijaRezultata % 7).setA1("/");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika1(postaviSlikuResenja("1"));
                }
                if (i == 1) {
                    listaRezultata.get(lokacijaRezultata % 7).setA2("/");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika2(postaviSlikuResenja("1"));
                }
                if (i == 2) {
                    listaRezultata.get(lokacijaRezultata % 7).setA3("/");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika3(postaviSlikuResenja("1"));
                }
                if (i == 3) {
                    listaRezultata.get(lokacijaRezultata % 7).setA4("/");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika4(postaviSlikuResenja("1"));
                }
                sadrzi--;
            }
            for (int i = tacno + sadrzi2; i < 4; i++) {
                if (i == 0) {
                    listaRezultata.get(lokacijaRezultata % 7).setA1("-");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika1(postaviSlikuResenja("2"));
                }
                if (i == 1) {
                    listaRezultata.get(lokacijaRezultata % 7).setA2("-");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika2(postaviSlikuResenja("2"));
                }
                if (i == 2) {
                    listaRezultata.get(lokacijaRezultata % 7).setA3("-");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika3(postaviSlikuResenja("2"));
                }
                if (i == 3) {
                    listaRezultata.get(lokacijaRezultata % 7).setA4("-");
                    listaRezultata.get(lokacijaRezultata % 7).setSlika4(postaviSlikuResenja("2"));
                }
            }
            lokacijaRezultata++;
            if (lokacija == 24) {
                master = true;
                gotovo = true;
                sledeciSimbol(niz[0]);
                sledeciSimbol(niz[1]);
                sledeciSimbol(niz[2]);
                sledeciSimbol(niz[3]);
                uslov = false;
                dodatniUslov = false;
            }
        }
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("IgradanaSkocko.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(slagalicaKontroler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void dalje() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("IgradanaSpojnice.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(slagalicaKontroler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isMaster() {
        return master;
    }

    public void setMaster(boolean master) {
        this.master = master;
    }

    public ArrayList<Kombinacija> getListaRezultata() {
        return listaRezultata;
    }

    public void setListaRezultata(ArrayList<Kombinacija> listaRezultata) {
        this.listaRezultata = listaRezultata;
    }

    public ArrayList<Kombinacija> getListaKombinacija() {
        return listaKombinacija;
    }

    public void setListaKombinacija(ArrayList<Kombinacija> listaKombinacija) {
        this.listaKombinacija = listaKombinacija;
    }

    public boolean isUslov() {
        return uslov;
    }

    public boolean isGotovo() {
        return gotovo;
    }

    public void setGotovo(boolean gotovo) {
        this.gotovo = gotovo;
    }

    public void setUslov(boolean uslov) {
        this.uslov = uslov;
    }

    public String getKombinacija() {
        return kombinacija;
    }

    public void setKombinacija(String kombinacija) {
        this.kombinacija = kombinacija;
    }

    public int getVreme() {
        return vreme;
    }

    public void setVreme(int vreme) {
        this.vreme = vreme;
    }

    public int getPoen() {
        return poen;
    }

    public void setPoen(int poen) {
        this.poen = poen;
    }

    public class Kombinacija {

        private String a1, a2, a3, a4;
        private String slika1, slika2, slika3, slika4;

        public String getSlika1() {
            return slika1;
        }

        public void setSlika1(String slika1) {
            this.slika1 = slika1;
        }

        public String getSlika2() {
            return slika2;
        }

        public void setSlika2(String slika2) {
            this.slika2 = slika2;
        }

        public String getSlika3() {
            return slika3;
        }

        public void setSlika3(String slika3) {
            this.slika3 = slika3;
        }

        public String getSlika4() {
            return slika4;
        }

        public void setSlika4(String slika4) {
            this.slika4 = slika4;
        }

        public String getA1() {
            return a1;
        }

        public void setA1(String a1) {
            this.a1 = a1;
        }

        public String getA2() {
            return a2;
        }

        public void setA2(String a2) {
            this.a2 = a2;
        }

        public String getA3() {
            return a3;
        }

        public void setA3(String a3) {
            this.a3 = a3;
        }

        public String getA4() {
            return a4;
        }

        public void setA4(String a4) {
            this.a4 = a4;
        }

    }
}
