/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kontroleri;

import db.HibernateUtil;
import entiteti.Igraasocijacije;
import entiteti.Igraslagalica;
import entiteti.Igraspojnice;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

@ManagedBean
@SessionScoped
public class SupervizorKontroler {

    private String rec;
    private ArrayList<Spojnica> listaSpojnica;
    private ArrayList<Kolona> listaKolona;
    private String konacnoResenje, sinonimi;
    private String poruka = "", poruka2 = "", poruka3 = "";

    public SupervizorKontroler() {
        listaSpojnica = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            listaSpojnica.add(new Spojnica());
        }
        listaKolona = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            listaKolona.add(new Kolona());
        }
    }

    public void dodajRec() {
        poruka = "";
        poruka2 = "";
        poruka3 = "";
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Igraslagalica slagalica = new Igraslagalica();
        slagalica.setRec(rec);
        session.save(slagalica);
        session.getTransaction().commit();
        session.close();
        poruka = "Reč je dodata!";
    }

    public void dodajSpojnicu() {
        poruka = "";
        poruka2 = "";
        poruka3 = "";
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        Criteria query = session.createCriteria(Igraspojnice.class);
        session.beginTransaction();
        ArrayList<Igraspojnice> temp = (ArrayList<Igraspojnice>) query.list();
        session.getTransaction().commit();
        int vrsta = temp.get(temp.size() - 1).getVrsta() + 1;
        for (Spojnica s : listaSpojnica) {
            session.beginTransaction();
            Igraspojnice spoj = new Igraspojnice();
            spoj.setPojam1(s.getSpoj1());
            spoj.setPojam2(s.getSpoj2());
            spoj.setVrsta(vrsta);
            session.save(spoj);
            session.getTransaction().commit();
        }
        session.close();
        poruka2 = "Uspešno dodato!";
    }

    public void dodajAsocijaciju() {
        poruka = "";
        poruka2 = "";
        poruka3 = "";
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Igraasocijacije asocijacije = new Igraasocijacije();
        String kolonaA = "", kolonaB = "", kolonaC = "", kolonaD = "";
        for (Kolona k : listaKolona) {
            kolonaA += k.polje1 + ", ";
            kolonaB += k.polje2 + ", ";
            kolonaC += k.polje3 + ", ";
            kolonaD += k.polje4 + ", ";
        }
        asocijacije.setKolonaA(kolonaA);
        asocijacije.setKolonaB(kolonaB);
        asocijacije.setKolonaC(kolonaC);
        asocijacije.setKolonaD(kolonaD);
        asocijacije.setKonacnoResenje(konacnoResenje);
        asocijacije.setSinonimi(sinonimi);
        session.save(asocijacije);
        session.getTransaction().commit();
        session.close();
        poruka3 = "Uspešno dodato!";
    }

    public String getKonacnoResenje() {
        return konacnoResenje;
    }

    public String getPoruka2() {
        return poruka2;
    }

    public void setPoruka2(String poruka2) {
        this.poruka2 = poruka2;
    }

    public String getPoruka3() {
        return poruka3;
    }

    public void setPoruka3(String poruka3) {
        this.poruka3 = poruka3;
    }

    public void setKonacnoResenje(String konacnoResenje) {
        this.konacnoResenje = konacnoResenje;
    }

    public String getPoruka() {
        return poruka;
    }

    public void setPoruka(String poruka) {
        this.poruka = poruka;
    }

    public String getSinonimi() {
        return sinonimi;
    }

    public void setSinonimi(String sinonimi) {
        this.sinonimi = sinonimi;
    }

    public ArrayList<Kolona> getListaKolona() {
        return listaKolona;
    }

    public void setListaKolona(ArrayList<Kolona> listaKolona) {
        this.listaKolona = listaKolona;
    }

    public ArrayList<Spojnica> getListaSpojnica() {
        return listaSpojnica;
    }

    public void setListaSpojnica(ArrayList<Spojnica> listaSpojnica) {
        this.listaSpojnica = listaSpojnica;
    }

    public String getRec() {
        return rec;
    }

    public void setRec(String rec) {
        this.rec = rec;
    }

    public class Kolona {

        private String polje1, polje2, polje3, polje4;

        public String getPolje1() {
            return polje1;
        }

        public void setPolje1(String polje1) {
            this.polje1 = polje1;
        }

        public String getPolje2() {
            return polje2;
        }

        public void setPolje2(String polje2) {
            this.polje2 = polje2;
        }

        public String getPolje3() {
            return polje3;
        }

        public void setPolje3(String polje3) {
            this.polje3 = polje3;
        }

        public String getPolje4() {
            return polje4;
        }

        public void setPolje4(String polje4) {
            this.polje4 = polje4;
        }
    }

    public class Spojnica {

        private String spoj1;
        private String spoj2;

        public String getSpoj1() {
            return spoj1;
        }

        public void setSpoj1(String spoj1) {
            this.spoj1 = spoj1;
        }

        public String getSpoj2() {
            return spoj2;
        }

        public void setSpoj2(String spoj2) {
            this.spoj2 = spoj2;
        }
    }
}
