/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kontroleri;

import db.HibernateUtil;
import entiteti.Igraslagalica;
import entiteti.Odigranipotezi;
import entiteti.Partije;
import entiteti.Registracija;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@SessionScoped
public class slagalicaKontroler implements Serializable {

    private int lokacija = 0;
    private String[] nizSlova = new String[]{"A", "B", "C", "Č", "Ć", "D", "Dž", "Đ", "E", "F", "G", "H", "I",
        "J", "K", "L", "Lj", "M", "N", "Nj", "O", "P", "R", "S", "Š", "T", "U", "V", "Z", "Ž", "A", "E", "I",
        "O", "U", "A", "E", "I", "O", "U"};
    private boolean uslov = false;
    private String slovo0, slovo1, slovo2, slovo3, slovo4, slovo5, slovo6,
            slovo7, slovo8, slovo9, slovo10, slovo11;
    private String konacnaRec, poeni1, poeni2;
    private int vreme = 60;
    private boolean gotovo = false, dodatniUslov = false, gotovaIgra = false;
    private int redniBrPartije = 0;
    private Registracija reg;
    private Partije partija = null;

    public void init() {
        if (gotovo == true) {
            gotovo = false;
            dodatniUslov = false;
            vreme = 60;
            slovo0 = "";
            slovo1 = "";
            slovo2 = "";
            slovo3 = "";
            slovo4 = "";
            slovo5 = "";
            slovo6 = "";
            slovo7 = "";
            slovo8 = "";
            slovo9 = "";
            slovo10 = "";
            slovo11 = "";
            konacnaRec = "";
        }
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession s = (HttpSession) fc.getExternalContext().getSession(false);
        reg = (Registracija) s.getAttribute("user");
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Partije.class);
        partija = (Partije) query.add(Restrictions.eq("idKorisnika1", reg.getId())).add(Restrictions.eq("status", new Byte("1"))).uniqueResult();
        session.getTransaction().commit();
        if (partija == null) {
            query = session.createCriteria(Partije.class);
            session.beginTransaction();
            partija = (Partije) query.add(Restrictions.eq("idKorisnika2", reg.getId())).add(Restrictions.eq("status", new Byte("1"))).uniqueResult();
            session.getTransaction().commit();
        }
        session.close();
        if (partija == null) {
            poeni1 = "0";
            poeni2 = "0";
            uslov = true;
        } else {
            if (partija.getPotez() != new Byte("2")) {
                if (partija.getIdKorisnika1() != reg.getId()) {
                    uslov = true;
                }
            }
            poeni1 = String.valueOf(partija.getPoeni1());
            poeni2 = String.valueOf(partija.getPoeni2());
        }
    }

    public int getVreme() {
        return vreme;
    }

    public void setVreme(int vreme) {
        this.vreme = vreme;
    }

    public String getPoeni1() {
        return poeni1;
    }

    public void setPoeni1(String poeni1) {
        this.poeni1 = poeni1;
    }

    public String getPoeni2() {
        return poeni2;
    }

    public void setPoeni2(String poeni2) {
        this.poeni2 = poeni2;
    }

    public String getKonacnaRec() {
        return konacnaRec;
    }

    public void setKonacnaRec(String konacnaRec) {
        this.konacnaRec = konacnaRec;
    }

    public String[] getNizSlova() {
        return nizSlova;
    }

    public void setNizSlova(String[] nizSlova) {
        this.nizSlova = nizSlova;
    }

    public String getSlovo0() {
        return slovo0;
    }

    public void setSlovo0(String slovo0) {
        this.slovo0 = slovo0;
    }

    public String getSlovo1() {
        return slovo1;
    }

    public void setSlovo1(String slovo1) {
        this.slovo1 = slovo1;
    }

    public String getSlovo2() {
        return slovo2;
    }

    public void setSlovo2(String slovo2) {
        this.slovo2 = slovo2;
    }

    public String getSlovo3() {
        return slovo3;
    }

    public void setSlovo3(String slovo3) {
        this.slovo3 = slovo3;
    }

    public String getSlovo4() {
        return slovo4;
    }

    public void setSlovo4(String slovo4) {
        this.slovo4 = slovo4;
    }

    public String getSlovo5() {
        return slovo5;
    }

    public void setSlovo5(String slovo5) {
        this.slovo5 = slovo5;
    }

    public String getSlovo6() {
        return slovo6;
    }

    public void setSlovo6(String slovo6) {
        this.slovo6 = slovo6;
    }

    public String getSlovo7() {
        return slovo7;
    }

    public void setSlovo7(String slovo7) {
        this.slovo7 = slovo7;
    }

    public String getSlovo8() {
        return slovo8;
    }

    public void setSlovo8(String slovo8) {
        this.slovo8 = slovo8;
    }

    public String getSlovo9() {
        return slovo9;
    }

    public boolean isGotovaIgra() {
        return gotovaIgra;
    }

    public void setGotovaIgra(boolean gotovaIgra) {
        this.gotovaIgra = gotovaIgra;
    }

    public void setSlovo9(String slovo9) {
        this.slovo9 = slovo9;
    }

    public String getSlovo10() {
        return slovo10;
    }

    public void setSlovo10(String slovo10) {
        this.slovo10 = slovo10;
    }

    public String getSlovo11() {
        return slovo11;
    }

    public int getLokacija() {
        return lokacija;
    }

    public void setLokacija(int lokacija) {
        this.lokacija = lokacija;
    }

    public void setSlovo11(String slovo11) {
        this.slovo11 = slovo11;
    }

    public boolean isUslov() {
        return uslov;
    }

    public void setUslov(boolean uslov) {
        this.uslov = uslov;
    }

    public void sledeceSlovo() {
        String temp = "";
        int br = new Random(System.currentTimeMillis()).nextInt(nizSlova.length);
        switch (lokacija) {
            case 0:
                slovo0 = nizSlova[br];
                temp = slovo0;
                break;
            case 1:
                slovo1 = nizSlova[br];
                temp = slovo1;
                break;
            case 2:
                slovo2 = nizSlova[br];
                temp = slovo2;
                break;
            case 3:
                slovo3 = nizSlova[br];
                temp = slovo3;
                break;
            case 4:
                slovo4 = nizSlova[br];
                temp = slovo4;
                break;
            case 5:
                slovo5 = nizSlova[br];
                temp = slovo5;
                break;
            case 6:
                slovo6 = nizSlova[br];
                temp = slovo6;
                break;
            case 7:
                slovo7 = nizSlova[br];
                temp = slovo7;
                break;
            case 8:
                slovo8 = nizSlova[br];
                temp = slovo8;
                break;
            case 9:
                slovo9 = nizSlova[br];
                temp = slovo9;
                break;
            case 10:
                slovo10 = nizSlova[br];
                temp = slovo10;
                break;
            case 11:
                slovo11 = nizSlova[br];
                temp = slovo11;
                break;
        }
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        Odigranipotezi odrigrano = new Odigranipotezi();
        odrigrano.setIdPartije(partija.getId());
        odrigrano.setIdKorisnika(reg.getId());
        odrigrano.setTipIgre(new Byte("1"));
        odrigrano.setStaJeUradjeno(temp);
        session.beginTransaction();
        session.save(odrigrano);
        session.getTransaction().commit();
        session.close();
        lokacija++;
        if (lokacija == 12) {
            uslov = true;
            dodatniUslov = true;
            lokacija = 0;
        }
    }

    public void proveravaj() {
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        if (partija == null) {
            session.beginTransaction();
            Criteria query = session.createCriteria(Partije.class);
            partija = (Partije) query.add(Restrictions.eq("idKorisnika1", reg.getId())).add(Restrictions.eq("status", new Byte("1"))).uniqueResult();
            session.getTransaction().commit();
            if (partija == null) {
                query = session.createCriteria(Partije.class);
                session.beginTransaction();
                partija = (Partije) query.add(Restrictions.eq("idKorisnika2", reg.getId())).add(Restrictions.eq("status", new Byte("1"))).uniqueResult();
                session.getTransaction().commit();
            }
        }
        Criteria query = session.createCriteria(Odigranipotezi.class);
        session.beginTransaction();
        ArrayList<Odigranipotezi> potez1 = (ArrayList<Odigranipotezi>) query.add(Restrictions.eq("idPartije", partija.getId()))
                .add(Restrictions.eq("tipIgre", new Byte("0"))).list();
        session.getTransaction().commit();
        if (potez1.size() == 2) {
            Odigranipotezi ewr = potez1.get(0);
            String ghj = potez1.get(0).getStaJeUradjeno();
            int poen1 = Integer.parseInt(potez1.get(0).getStaJeUradjeno());
            int poen2 = Integer.parseInt(potez1.get(1).getStaJeUradjeno());
            if (poen1 > poen2) {
                poen2 = 0;
            } else if (poen1 < poen2) {
                poen1 = 0;
            }
            if (potez1.get(0).getIdKorisnika() == partija.getIdKorisnika1()) {
                partija.setPoeni1(partija.getPoeni1() + poen1);
                partija.setPoeni2(partija.getPoeni2() + poen2);
            } else {
                partija.setPoeni1(poen2 + partija.getPoeni1());
                partija.setPoeni2(poen1 + partija.getPoeni2());
            }
            session.beginTransaction();
            session.update(partija);
            session.getTransaction().commit();
            session.beginTransaction();
            Odigranipotezi pote = (Odigranipotezi) session.merge(potez1.get(0));
            session.delete(pote);
            session.getTransaction().commit();
            session.beginTransaction();
            pote = (Odigranipotezi) session.merge(potez1.get(1));
            session.delete(pote);
            session.getTransaction().commit();
        }
        boolean radi = false;
        poeni1 = String.valueOf(partija.getPoeni1());
        poeni2 = String.valueOf(partija.getPoeni2());
        if (partija.getPotez() == new Byte("1")) {
            if (partija.getIdKorisnika1() != reg.getId()) {
                radi = true;
            }
        } else {
            if (partija.getIdKorisnika2() != reg.getId()) {
                radi = true;
            }
        }
        if (radi) {
            query = session.createCriteria(Odigranipotezi.class);
            session.beginTransaction();
            ArrayList<Odigranipotezi> potez = (ArrayList<Odigranipotezi>) query.add(Restrictions.eq("idPartije", partija.getId()))
                    .add(Restrictions.eq("tipIgre", new Byte("1"))).list();
            session.getTransaction().commit();
            for (int i = 0; i < potez.size(); i++) {
                switch (lokacija) {
                    case 0:
                        slovo0 = potez.get(i).getStaJeUradjeno();
                        break;
                    case 1:
                        slovo1 = potez.get(i).getStaJeUradjeno();
                        break;
                    case 2:
                        slovo2 = potez.get(i).getStaJeUradjeno();
                        break;
                    case 3:
                        slovo3 = potez.get(i).getStaJeUradjeno();
                        break;
                    case 4:
                        slovo4 = potez.get(i).getStaJeUradjeno();
                        break;
                    case 5:
                        slovo5 = potez.get(i).getStaJeUradjeno();
                        break;
                    case 6:
                        slovo6 = potez.get(i).getStaJeUradjeno();
                        break;
                    case 7:
                        slovo7 = potez.get(i).getStaJeUradjeno();
                        break;
                    case 8:
                        slovo8 = potez.get(i).getStaJeUradjeno();
                    case 9:
                        slovo9 = potez.get(i).getStaJeUradjeno();
                        break;
                    case 10:
                        slovo10 = potez.get(i).getStaJeUradjeno();
                        break;
                    case 11:
                        slovo11 = potez.get(i).getStaJeUradjeno();
                        break;
                }
                lokacija++;
                if (lokacija == 12) {
                    lokacija = -1;
                    dodatniUslov = true;
                    break;
                }

                session.beginTransaction();
                Odigranipotezi pote = (Odigranipotezi) session.merge(potez.get(i));
                session.delete(pote);
                session.getTransaction().commit();
            }
        }
        session.close();
    }

    public void promenaVremena() {
        if (dodatniUslov) {
            if (vreme >= 0) {
                vreme--;
                if (vreme == 0) {
                    if (!gotovo) {
                        predajRec();
                    }
                }
            }
        }
    }

    public void predajRec() {
        dodatniUslov = false;
        gotovo = true;
        uslov = false;

        ArrayList<String> slova = new ArrayList<>();
        slova.add(slovo0);
        slova.add(slovo1);
        slova.add(slovo2);
        slova.add(slovo3);
        slova.add(slovo4);
        slova.add(slovo5);
        slova.add(slovo6);
        slova.add(slovo7);
        slova.add(slovo8);
        slova.add(slovo9);
        slova.add(slovo10);
        slova.add(slovo11);

        int osvojeniPoeni = 0;
        konacnaRec = konacnaRec.toUpperCase();
        for (int i = 0; i < konacnaRec.length(); i++) {
            String temp = "" + konacnaRec.charAt(i);
            if (slova.contains(temp)) {
                osvojeniPoeni += 2;
                slova.remove(temp);
            } else {
                osvojeniPoeni = 0;
                break;
            }
        }
        if (!proveriPostojiRec(konacnaRec)) {
            osvojeniPoeni = 0;
        }
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        Odigranipotezi odigrani = new Odigranipotezi();
        odigrani.setIdKorisnika(reg.getId());
        odigrani.setIdPartije(partija.getId());
        odigrani.setTipIgre(new Byte("0"));
        odigrani.setStaJeUradjeno(String.valueOf(osvojeniPoeni));
        session.beginTransaction();
        session.save(odigrani);
        session.getTransaction().commit();

        if (redniBrPartije == 0) {
            redniBrPartije++;
            if (partija.getIdKorisnika1() == reg.getId()) {
                uslov = true;
            }
            if (partija.getPotez() != new Byte("2")) {
                partija.setPotez(new Byte("2"));
                session.beginTransaction();
                session.update(partija);
                session.getTransaction().commit();
            }
            Criteria query = session.createCriteria(Odigranipotezi.class);
            session.beginTransaction();
            ArrayList<Odigranipotezi> potez = (ArrayList<Odigranipotezi>) query.add(Restrictions.eq("idPartije", partija.getId()))
                    .add(Restrictions.eq("tipIgre", new Byte("1"))).list();
            session.getTransaction().commit();
            for (int i = 0; i < potez.size(); i++) {
                session.beginTransaction();
                Odigranipotezi pote = (Odigranipotezi) session.merge(potez.get(i));
                session.delete(pote);
                session.getTransaction().commit();
            }
            init();
        } else {
            proveravaj();
            //mojbroj
            partija.setPotez(new Byte("1"));
            session.beginTransaction();
            session.update(partija);
            session.getTransaction().commit();
            gotovaIgra = true;
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("mojBroj.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(slagalicaKontroler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        session.close();
    }

    private boolean proveriPostojiRec(String rec) {//proveri u bazi reci, ili dozvoli supervizoru
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Igraslagalica.class);
        Igraslagalica igra = (Igraslagalica) query.add(Restrictions.eq("rec", rec.toLowerCase())).setMaxResults(1).uniqueResult();
        session.getTransaction().commit();
        session.close();
        if (igra == null) {
            return false;
        }
        return true;
    }
}
