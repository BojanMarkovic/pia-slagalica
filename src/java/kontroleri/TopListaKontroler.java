/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kontroleri;

import db.HibernateUtil;
import entiteti.Partije;
import entiteti.Registracija;
import java.util.Date;
import java.util.ArrayList;
import java.util.Collections;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

@ManagedBean
@RequestScoped
public class TopListaKontroler {

    public TopListaKontroler() {
        listaKorisnika7Dana = new ArrayList<>();
        listaKorisnikMesecDana = new ArrayList<>();
        inicijalizuj();
    }

    private void inicijalizuj() {
        listaKorisnika7Dana = new ArrayList<>();
        listaKorisnikMesecDana = new ArrayList<>();
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Partije.class);
        ArrayList<Partije> temp = (ArrayList<Partije>) query.add(Restrictions.eq("status", new Byte("2"))).list();
        session.getTransaction().commit();
        session.close();

        long DAY_IN_MS = 1000 * 60 * 60 * 24;
        Date datumPre7 = new Date(System.currentTimeMillis() - (7 * DAY_IN_MS));
        Date datumPreMesec = new Date(System.currentTimeMillis());
        datumPreMesec.setHours(0);
        datumPreMesec.setMinutes(0);
        datumPreMesec.setSeconds(0);
        datumPreMesec.setDate(1);

        for (int i = 0; i < temp.size(); i++) {
            if (datumPre7.getTime() < temp.get(i).getDatum().getTime()) {
                topKorisnici kor = new topKorisnici();
                kor.setId(temp.get(i).getIdKorisnika1());
                kor.setPoeni(temp.get(i).getPoeni1());
                dodajAkoMozes7(kor);
                dodajAkoMozesMesec(kor);
                kor = new topKorisnici();
                kor.setId(temp.get(i).getIdKorisnika2());
                kor.setPoeni(temp.get(i).getPoeni2());
                dodajAkoMozes7(kor);
                dodajAkoMozesMesec(kor);
            } else if ((datumPre7.getTime() >= temp.get(i).getDatum().getTime())
                    && (datumPreMesec.getTime() < temp.get(i).getDatum().getTime())) {
                topKorisnici kor = new topKorisnici();
                kor.setId(temp.get(i).getIdKorisnika1());
                kor.setPoeni(temp.get(i).getPoeni1());
                dodajAkoMozesMesec(kor);
                kor = new topKorisnici();
                kor.setId(temp.get(i).getIdKorisnika2());
                kor.setPoeni(temp.get(i).getPoeni2());
                dodajAkoMozesMesec(kor);
            }
        }
        sortiraj();
        popuni();
    }

    private void sortiraj() {
        for (int i = 0; i < (listaKorisnika7Dana.size() - 1); i++) {
            for (int j = i + 1; j < listaKorisnika7Dana.size(); j++) {
                if (listaKorisnika7Dana.get(i).getPoeni() < listaKorisnika7Dana.get(j).getPoeni()) {
                    Collections.swap(listaKorisnika7Dana, i, j);
                }
            }
        }
        for (int i = 0; i < (listaKorisnikMesecDana.size() - 1); i++) {
            for (int j = i + 1; j < listaKorisnikMesecDana.size(); j++) {
                if (listaKorisnikMesecDana.get(i).getPoeni() < listaKorisnikMesecDana.get(j).getPoeni()) {
                    Collections.swap(listaKorisnikMesecDana, i, j);
                }
            }
        }
    }

    private void popuni() {
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        for (int i = 0; i < listaKorisnika7Dana.size(); i++) {
            session.beginTransaction();
            Criteria query = session.createCriteria(Registracija.class);
            Registracija user = (Registracija) query.add(Restrictions.eq("id", listaKorisnika7Dana.get(i).getId())).uniqueResult();
            session.getTransaction().commit();
            listaKorisnika7Dana.get(i).setKorisnik(user);
            listaKorisnika7Dana.get(i).setRedniBr(i + 1);
            listaKorisnika7Dana.get(i).setKorIme(user.getKorisnickoIme());
        }
        for (int i = 0; i < listaKorisnikMesecDana.size(); i++) {
            session.beginTransaction();
            Criteria query = session.createCriteria(Registracija.class);
            Registracija user = (Registracija) query.add(Restrictions.eq("id", listaKorisnikMesecDana.get(i).getId())).uniqueResult();
            session.getTransaction().commit();
            listaKorisnikMesecDana.get(i).setKorisnik(user);
            listaKorisnikMesecDana.get(i).setRedniBr(i + 1);
            listaKorisnikMesecDana.get(i).setKorIme(user.getKorisnickoIme());
        }
        session.close();
    }

    public class topKorisnici {

        private int redniBr;
        private String korIme;
        private Registracija korisnik;
        private int id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Registracija getKorisnik() {
            return korisnik;
        }

        public void setKorisnik(Registracija korisnik) {
            this.korisnik = korisnik;
        }

        public String getKorIme() {
            return korIme;
        }

        public void setKorIme(String korIme) {
            this.korIme = korIme;
        }
        private int poeni;

        public int getRedniBr() {
            return redniBr;
        }

        public void setRedniBr(int redniBr) {
            this.redniBr = redniBr;
        }

        public int getPoeni() {
            return poeni;
        }

        public void setPoeni(int poeni) {
            this.poeni = poeni;
        }
    }

    private void dodajAkoMozes7(topKorisnici temp) {
        for (topKorisnici t : listaKorisnika7Dana) {
            if (t.getId() == temp.getId()) {
                t.setPoeni(t.getPoeni() + temp.getPoeni());
                return;
            }
        }
        listaKorisnika7Dana.add(temp);
    }

    private void dodajAkoMozesMesec(topKorisnici temp) {
        for (topKorisnici t : listaKorisnikMesecDana) {
            if (t.getId() == temp.getId()) {
                t.setPoeni(t.getPoeni() + temp.getPoeni());
                return;
            }
        }
        listaKorisnikMesecDana.add(temp);
    }

    private static ArrayList<topKorisnici> listaKorisnika7Dana;
    private static ArrayList<topKorisnici> listaKorisnikMesecDana;

    public ArrayList<topKorisnici> getListaKorisnika7Dana() {
        return listaKorisnika7Dana;
    }

    public void setListaKorisnika7Dana(ArrayList<topKorisnici> listaKorisnika7Dana) {
        this.listaKorisnika7Dana = listaKorisnika7Dana;
    }

    public ArrayList<topKorisnici> getListaKorisnikMesecDana() {
        return listaKorisnikMesecDana;
    }

    public void setListaKorisnikMesecDana(ArrayList<topKorisnici> listaKorisnikMesecDana) {
        this.listaKorisnikMesecDana = listaKorisnikMesecDana;
    }

}
