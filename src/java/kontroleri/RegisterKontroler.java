/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kontroleri;

import db.HibernateUtil;
import entiteti.Registracija;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.imageio.ImageIO;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.primefaces.event.FileUploadEvent;
import org.apache.commons.io.IOUtils;
import org.primefaces.model.UploadedFile;

@ManagedBean
@RequestScoped
public class RegisterKontroler implements Serializable {

    private String ime;
    private String prezime;
    private String korisnickoIme;
    private String lozinka;
    private String potvrdaLozinke;
    private String email;
    private String zanimanje;
    private String pol;
    private byte statusIgreDana;
    private byte tip;
    private byte[] slika;
    private UploadedFile fajl;
    private Date datum;
    private String novaLozinka;
    private String poruka1;
    private String poruka2;

    public void register() {
        if (fajl == null) {
            poruka1 = "Greška prilikom uploadovanja fajla!";
            return;
        }
        SessionFactory sessionF = HibernateUtil.getSessionFactory();
        Session session = sessionF.openSession();
        session.beginTransaction();
        Criteria query = session.createCriteria(Registracija.class);
        Registracija user = (Registracija) query.add(Restrictions.eq("korisnickoIme", korisnickoIme)).uniqueResult();
        session.getTransaction().commit();
        if (user != null) {
            poruka1 = "Korisnik vec postoji!";
        } else {
            try {
                byte[] image = fajl.getContents();
                BufferedImage bi = ImageIO.read(new ByteArrayInputStream(image));
                int width = bi.getWidth();
                int height = bi.getHeight();
                if ((width > 300) || (height > 300)) {
                    poruka1 = "Slika je prevelika!";
                    return;
                }
                poruka1 = "";
                Registracija registracija = new Registracija();
                registracija.setDatum(datum);
                registracija.setEmail(email);
                registracija.setKorisnickoIme(korisnickoIme);;
                registracija.setIme(ime);
                registracija.setPrezime(prezime);
                MessageDigest md = MessageDigest.getInstance("MD5");
                md.update(lozinka.getBytes());
                byte[] bytes = md.digest();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < bytes.length; i++) {
                    sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
                }
                String tempLozinka = sb.toString();
                registracija.setLozinka(tempLozinka);
                registracija.setZanimanje(zanimanje);
                registracija.setPol(pol);
                registracija.setStatusIgreDana(new Byte("2"));
                registracija.setTip(new Byte("4"));
                slika = IOUtils.toByteArray(fajl.getInputstream());
                registracija.setSlika(slika);
                session.beginTransaction();
                session.save(registracija);
                session.getTransaction().commit();
                poruka1 = "Korisnik je uspešno registrovan!";
            } catch (NoSuchAlgorithmException | IOException ex) {
                Logger.getLogger(RegisterKontroler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        session.close();

    }

    public void promeniLozinku() {
        try {
            SessionFactory sessionF = HibernateUtil.getSessionFactory();
            Session session = sessionF.openSession();
            session.beginTransaction();
            Criteria query = session.createCriteria(Registracija.class);
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(lozinka.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            lozinka = sb.toString();
            Registracija user = (Registracija) query.add(Restrictions.eq("korisnickoIme", korisnickoIme)).add(Restrictions.eq("lozinka", lozinka)).uniqueResult();
            session.getTransaction().commit();
            if (user == null) {
                poruka2 = "Korisnik ne postoji!";
            } else {
                poruka2 = "";
                user.setLozinka(novaLozinka);
                session.beginTransaction();
                session.update(user);
                session.getTransaction().commit();
                poruka2 = "Korisniku je uspešno promenjena lozinka!";
            }
            session.close();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(RegisterKontroler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void upload(FileUploadEvent event) {
        System.out.println("kontroleri.RegisterKontroler.upload()");
        fajl = event.getFile();
    }

    public UploadedFile getFajl() {
        return fajl;
    }

    public void setFajl(UploadedFile fajl) {
        System.out.println(new Date());
        this.fajl = fajl;
    }

    public String getPoruka1() {
        return poruka1;
    }

    public void setPoruka1(String poruka1) {
        this.poruka1 = poruka1;
    }

    public String getPoruka2() {
        return poruka2;
    }

    public void setPoruka2(String poruka2) {
        this.poruka2 = poruka2;
    }

    public String getNovaLozinka() {
        return novaLozinka;
    }

    public void setNovaLozinka(String novaLozinka) {
        this.novaLozinka = novaLozinka;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public String getPol() {
        return pol;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPotvrdaLozinke() {
        return potvrdaLozinke;
    }

    public void setPotvrdaLozinke(String potvrdaLozinke) {
        this.potvrdaLozinke = potvrdaLozinke;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getZanimanje() {
        return zanimanje;
    }

    public void setZanimanje(String zanimanje) {
        this.zanimanje = zanimanje;
    }

    public byte getStatusIgreDana() {
        return statusIgreDana;
    }

    public void setStatusIgreDana(byte statusIgreDana) {
        this.statusIgreDana = statusIgreDana;
    }

    public byte getTip() {
        return tip;
    }

    public void setTip(byte tip) {
        this.tip = tip;
    }

    public byte[] getSlika() {
        return slika;
    }

    public void setSlika(byte[] slika) {
        this.slika = slika;
    }

}
