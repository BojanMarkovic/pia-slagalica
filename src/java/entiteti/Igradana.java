package entiteti;
// Generated Jun 21, 2019 8:17:21 PM by Hibernate Tools 4.3.1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Igradana implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "idKorisnika")
    private int idKorisnika;
    @Column(name = "poeni")
    private int poeni;
    @Column(name = "datum")
    private Date datum;
    @Column(name = "poeniSlagalica")
    private int poeniiSlagalica;
    @Column(name = "poeniMojBroj")
    private int poeniMojBroj;
    @Column(name = "poeniSkocko")
    private int poeniSkocko;
    @Column(name = "poeniSpojnice")
    private int poeniSpojnice;
    @Column(name = "poeniAsocijacije")
    private int poeniAsocijacije;

    public int getPoeniiSlagalica() {
        return poeniiSlagalica;
    }

    public void setPoeniiSlagalica(int poeniiSlagalica) {
        this.poeniiSlagalica = poeniiSlagalica;
    }

    public int getPoeniMojBroj() {
        return poeniMojBroj;
    }

    public void setPoeniMojBroj(int poeniMojBroj) {
        this.poeniMojBroj = poeniMojBroj;
    }

    public int getPoeniSkocko() {
        return poeniSkocko;
    }

    public void setPoeniSkocko(int poeniSkocko) {
        this.poeniSkocko = poeniSkocko;
    }

    public int getPoeniSpojnice() {
        return poeniSpojnice;
    }

    public void setPoeniSpojnice(int poeniSpojnice) {
        this.poeniSpojnice = poeniSpojnice;
    }

    public int getPoeniAsocijacije() {
        return poeniAsocijacije;
    }

    public void setPoeniAsocijacije(int poeniAsocijacije) {
        this.poeniAsocijacije = poeniAsocijacije;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdKorisnika() {
        return idKorisnika;
    }

    public void setIdKorisnika(int idKorisnika) {
        this.idKorisnika = idKorisnika;
    }

    public int getPoeni() {
        return poeni;
    }

    public void setPoeni(int poeni) {
        this.poeni = poeni;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

}
