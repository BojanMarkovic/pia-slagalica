package entiteti;
// Generated Jun 21, 2019 8:17:21 PM by Hibernate Tools 4.3.1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Partije implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "idKorisnika2")
    private int idKorisnika2;
    @Column(name = "idKorisnika1")
    private int idKorisnika1;
    @Column(name = "poeni1")
    private int poeni1;
    @Column(name = "poeni2")
    private int poeni2;
    @Column(name = "datum")
    private Date datum;
    @Column(name = "status")
    private byte status;
    @Column(name = "potez")
    private byte potez;
    @Column(name = "poeniSlagalica1")
    private int poeniiSlagalica1;
    @Column(name = "poeniMojBroj1")
    private int poeniMojBroj1;
    @Column(name = "poeniSkocko1")
    private int poeniSkocko1;
    @Column(name = "poeniSpojnice1")
    private int poeniSpojnice1;
    @Column(name = "poeniAsocijacije1")
    private int poeniAsocijacije1;
    @Column(name = "poeniSlagalica2")
    private int poeniiSlagalica2;
    @Column(name = "poeniMojBroj2")
    private int poeniMojBroj2;
    @Column(name = "poeniSkocko2")
    private int poeniSkocko2;
    @Column(name = "poeniSpojnice2")
    private int poeniSpojnice2;
    @Column(name = "poeniAsocijacije2")
    private int poeniAsocijacije2;

    public int getPoeniiSlagalica1() {
        return poeniiSlagalica1;
    }

    public void setPoeniiSlagalica1(int poeniiSlagalica1) {
        this.poeniiSlagalica1 = poeniiSlagalica1;
    }

    public int getPoeniMojBroj1() {
        return poeniMojBroj1;
    }

    public void setPoeniMojBroj1(int poeniMojBroj1) {
        this.poeniMojBroj1 = poeniMojBroj1;
    }

    public int getPoeniSkocko1() {
        return poeniSkocko1;
    }

    public void setPoeniSkocko1(int poeniSkocko1) {
        this.poeniSkocko1 = poeniSkocko1;
    }

    public int getPoeniSpojnice1() {
        return poeniSpojnice1;
    }

    public void setPoeniSpojnice1(int poeniSpojnice1) {
        this.poeniSpojnice1 = poeniSpojnice1;
    }

    public int getPoeniAsocijacije1() {
        return poeniAsocijacije1;
    }

    public void setPoeniAsocijacije1(int poeniAsocijacije1) {
        this.poeniAsocijacije1 = poeniAsocijacije1;
    }

    public int getPoeniiSlagalica2() {
        return poeniiSlagalica2;
    }

    public void setPoeniiSlagalica2(int poeniiSlagalica2) {
        this.poeniiSlagalica2 = poeniiSlagalica2;
    }

    public int getPoeniMojBroj2() {
        return poeniMojBroj2;
    }

    public void setPoeniMojBroj2(int poeniMojBroj2) {
        this.poeniMojBroj2 = poeniMojBroj2;
    }

    public int getPoeniSkocko2() {
        return poeniSkocko2;
    }

    public void setPoeniSkocko2(int poeniSkocko2) {
        this.poeniSkocko2 = poeniSkocko2;
    }

    public int getPoeniSpojnice2() {
        return poeniSpojnice2;
    }

    public void setPoeniSpojnice2(int poeniSpojnice2) {
        this.poeniSpojnice2 = poeniSpojnice2;
    }

    public int getPoeniAsocijacije2() {
        return poeniAsocijacije2;
    }

    public void setPoeniAsocijacije2(int poeniAsocijacije2) {
        this.poeniAsocijacije2 = poeniAsocijacije2;
    }

    public byte getPotez() {
        return potez;
    }

    public void setPotez(byte potez) {
        this.potez = potez;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdKorisnika2() {
        return idKorisnika2;
    }

    public void setIdKorisnika2(int idKorisnika2) {
        this.idKorisnika2 = idKorisnika2;
    }

    public int getIdKorisnika1() {
        return idKorisnika1;
    }

    public void setIdKorisnika1(int idKorisnika1) {
        this.idKorisnika1 = idKorisnika1;
    }

    public int getPoeni1() {
        return poeni1;
    }

    public void setPoeni1(int poeni1) {
        this.poeni1 = poeni1;
    }

    public int getPoeni2() {
        return poeni2;
    }

    public void setPoeni2(int poeni2) {
        this.poeni2 = poeni2;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public byte getStatus() {
        return status;
    }

    public void setStatus(byte status) {
        this.status = status;
    }

}
