package entiteti;
// Generated Jun 21, 2019 8:17:21 PM by Hibernate Tools 4.3.1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Odigranipotezi implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "idPartije")
    private int idPartije;
    @Column(name = "idKorisnika")
    private int idKorisnika;
    @Column(name = "tipIgre")
    private byte tipIgre;
    @Column(name = "staJeUradjeno")
    private String staJeUradjeno;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdPartije() {
        return idPartije;
    }

    public void setIdPartije(int idPartije) {
        this.idPartije = idPartije;
    }

    public int getIdKorisnika() {
        return idKorisnika;
    }

    public void setIdKorisnika(int idKorisnika) {
        this.idKorisnika = idKorisnika;
    }

    public byte getTipIgre() {
        return tipIgre;
    }

    public void setTipIgre(byte tipIgre) {
        this.tipIgre = tipIgre;
    }

    public String getStaJeUradjeno() {
        return staJeUradjeno;
    }

    public void setStaJeUradjeno(String staJeUradjeno) {
        this.staJeUradjeno = staJeUradjeno;
    }

}
