package entiteti;
// Generated Jun 21, 2019 6:57:09 PM by Hibernate Tools 4.3.1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class Registracija implements java.io.Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "ime")
    private String ime;
    @Column(name = "prezime")
    private String prezime;
    @Column(name = "korisnickoIme")
    private String korisnickoIme;
    @Column(name = "lozinka")
    private String lozinka;
    @Column(name = "email")
    private String email;
    @Column(name = "zanimanje")
    private String zanimanje;
    @Column(name = "statusIgreDana")
    private byte statusIgreDana;
    @Column(name = "pol")
    private String pol;
    @Column(name = "tip")
    private byte tip;
    @Column(name = "datum")
    private Date datum;
    @Column(name = "slika")
    @Lob
    private byte[] slika;

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public String getPol() {
        return pol;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIme() {
        return this.ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return this.prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getKorisnickoIme() {
        return this.korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getLozinka() {
        return this.lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getZanimanje() {
        return this.zanimanje;
    }

    public void setZanimanje(String zanimanje) {
        this.zanimanje = zanimanje;
    }

    public byte getStatusIgreDana() {
        return this.statusIgreDana;
    }

    public void setStatusIgreDana(byte statusIgreDana) {
        this.statusIgreDana = statusIgreDana;
    }

    public byte getTip() {
        return this.tip;
    }

    public void setTip(byte tip) {
        this.tip = tip;
    }

    public byte[] getSlika() {
        return slika;
    }

    public void setSlika(byte[] slika) {
        this.slika = slika;
    }

}
