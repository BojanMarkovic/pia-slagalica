package entiteti;
// Generated Jun 21, 2019 8:17:21 PM by Hibernate Tools 4.3.1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Igraspojnice implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "pojam1")
    private String pojam1;
    @Column(name = "pojam2")
    private String pojam2;
    @Column(name = "vrsta")
    private int vrsta;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPojam1() {
        return pojam1;
    }

    public void setPojam1(String pojam1) {
        this.pojam1 = pojam1;
    }

    public String getPojam2() {
        return pojam2;
    }

    public void setPojam2(String pojam2) {
        this.pojam2 = pojam2;
    }

    public int getVrsta() {
        return vrsta;
    }

    public void setVrsta(int vrsta) {
        this.vrsta = vrsta;
    }

}
