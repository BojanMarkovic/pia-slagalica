package entiteti;
// Generated Jun 21, 2019 8:17:21 PM by Hibernate Tools 4.3.1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Igraasocijacije implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "kolonaA")
    private String kolonaA;
    @Column(name = "kolonaB")
    private String kolonaB;
    @Column(name = "kolonaC")
    private String kolonaC;
    @Column(name = "kolonaD")
    private String kolonaD;
    @Column(name = "konacnoResenje")
    private String konacnoResenje;
    @Column(name = "sinonimi")
    private String sinonimi;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKolonaA() {
        return kolonaA;
    }

    public void setKolonaA(String kolonaA) {
        this.kolonaA = kolonaA;
    }

    public String getKolonaB() {
        return kolonaB;
    }

    public void setKolonaB(String kolonaB) {
        this.kolonaB = kolonaB;
    }

    public String getKolonaC() {
        return kolonaC;
    }

    public void setKolonaC(String kolonaC) {
        this.kolonaC = kolonaC;
    }

    public String getKolonaD() {
        return kolonaD;
    }

    public void setKolonaD(String kolonaD) {
        this.kolonaD = kolonaD;
    }

    public String getKonacnoResenje() {
        return konacnoResenje;
    }

    public void setKonacnoResenje(String konacnoResenje) {
        this.konacnoResenje = konacnoResenje;
    }

    public String getSinonimi() {
        return sinonimi;
    }

    public void setSinonimi(String sinonimi) {
        this.sinonimi = sinonimi;
    }

}
