package entiteti;
// Generated Jun 21, 2019 8:17:21 PM by Hibernate Tools 4.3.1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Definisanjeigradana implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "idAsocijacije")
    private int idAsocijacije;
    @Column(name = "datum")
    private Date datum;
    @Column(name = "vrstaSpojnica")
    private int vrstaSpojnica;
    @Column(name = "seedMojBroj")
    private int seedMojBroj;
    @Column(name = "seedSlagalica")
    private int seedSlagalica;
    @Column(name = "seedSkocko")
    private int seedSkocko;

    public int getSeedMojBroj() {
        return seedMojBroj;
    }

    public void setSeedMojBroj(int seedMojBroj) {
        this.seedMojBroj = seedMojBroj;
    }

    public int getSeedSlagalica() {
        return seedSlagalica;
    }

    public void setSeedSlagalica(int seedSlagalica) {
        this.seedSlagalica = seedSlagalica;
    }

    public int getSeedSkocko() {
        return seedSkocko;
    }

    public void setSeedSkocko(int seedSkocko) {
        this.seedSkocko = seedSkocko;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdAsocijacije() {
        return idAsocijacije;
    }

    public void setIdAsocijacije(int idAsocijacije) {
        this.idAsocijacije = idAsocijacije;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public int getVrstaSpojnica() {
        return vrstaSpojnica;
    }

    public void setVrstaSpojnica(int vrstaSpojnica) {
        this.vrstaSpojnica = vrstaSpojnica;
    }

}
